/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.PageObjects.FR1_RiskRegister_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.PageObjects.FR2_FR10_RiskRegister_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.PageObjects.TrainingSiteHomePageObjects;
import org.openqa.selenium.JavascriptExecutor;


/**
 *
 * @author Vijaya
 */
@KeywordAnnotation(
        Keyword = "FR3_ConductRiskAssessment_withoutBowtie_AlernateScenario1",
        createNewBrowserInstance = false
)
public class FR3_ConductRiskAssessment_withoutBowtie_AlernateScenario1 extends BaseClass {

    String error = "";

    public FR3_ConductRiskAssessment_withoutBowtie_AlernateScenario1() {

    }

    public TestResult executeTest() {

        if (!addRiskAssessment()) {
            return narrator.testFailed("Failed to edit Risk Assessment record - " + error);
        }
        
        return narrator.finalizeTest("Successfully edited Risk Assessment record");
    }

    public boolean addRiskAssessment() {

       
        //Processflow
//        pause(15000);
//       if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.riskAssessmentProcessflow())){
//            error = "Failed to wait for 'Processflow' button.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.riskAssessmentProcessflow())){
//            error = "Failed to click on 'Processflow' button.";
//            return false;
//        }
//        
//        narrator.stepPassedWithScreenShot("Record opens in Inprogress phase");
//        String processStatus2 = SeleniumDriverInstance.retrieveAttributeByXpath(FR1_RiskRegister_PageObjects.processFlowStatus("2. In Progress"),"class");
//        if (!processStatus2.equalsIgnoreCase("step active")) {
//        error = "Failed to move to  2. In Progress phase";
//        return false;
//        }
        
        //Edit Risk description
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.riskDescription())) {
            error = "Failed to wait for Risk description";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(FR2_FR10_RiskRegister_PageObjects.riskDescription(), getData("Edit Risk description"))) {
            error = "Failed to edit Risk description";
            return false;
        } 
        
        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.saveRiskAssessmentButton())) {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.saveRiskAssessmentButton())) {
            error = "Failed to click on 'Save' button.";
            return false;
        }

         if (!SeleniumDriverInstance.WaitForLoaderToDisappear(FR1_RiskRegister_PageObjects.saveWait())) {
            error = "Failed to wait for 'Save wait' button.";
            return false;
        }

      
        //Validate save
        if (!SeleniumDriverInstance.waitForElementsByXpath(FR1_RiskRegister_PageObjects.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(FR1_RiskRegister_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");

        
        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(FR2_FR10_RiskRegister_PageObjects.riskAssessmentRecordNumber()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());
        
        return true;
    }

}
