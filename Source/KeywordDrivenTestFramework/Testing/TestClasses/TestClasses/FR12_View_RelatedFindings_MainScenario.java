/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.PageObjects.FR1_RiskRegister_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.PageObjects.FR2_FR10_RiskRegister_PageObjects;
import org.openqa.selenium.JavascriptExecutor;


/**
 *
 * @author Vijaya
 */
@KeywordAnnotation(
        Keyword = "FR12_View_RelatedFindings_MainScenario",
        createNewBrowserInstance = false
)
public class FR12_View_RelatedFindings_MainScenario extends BaseClass {

    String error = "";

    public FR12_View_RelatedFindings_MainScenario() {

    }

    public TestResult executeTest() {

        if (!viewRelatedFindings()) {
            return narrator.testFailed("Failed to view Related Findings - " + error);
        }
        
        return narrator.finalizeTest("Successfully viewed Related Findings");
    }

    public boolean viewRelatedFindings() {
        
        //Related Findings panel
         if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.relatedFindingsPanel(), 5000)){
            error = "Failed to wait for Related Findings panel";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.relatedFindingsPanel())){
            error = "Failed to click on Related Findings panel";
            return false;
        }
        
        //Scroll down
        JavascriptExecutor js = (JavascriptExecutor)SeleniumDriverInstance.Driver;
        js.executeScript("window.scrollBy(0,1000)");


        SeleniumDriverInstance.pause(20000);
        narrator.stepPassedWithScreenShot("Findings view grid displayed");
        
        //Record to be opened
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.findingsRecordOpen(), 5000)){
            error = "Failed to wait for Findings record to be opened";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.findingsRecordOpen())){
            error = "Failed to click on Findings record to be opened";
            return false;
        }
        
        pause(60000);
        
        //Findings Processflow
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.findingsRecordOpenProcessflow())){
            error = "Failed to wait for 'Processflow' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.findingsRecordOpenProcessflow())){
            error = "Failed to click on 'Processflow' button.";
            return false;
        } 
        narrator.stepPassedWithScreenShot("Findings record opens in Edit Phase");
        
        
        return true;
    }

}
