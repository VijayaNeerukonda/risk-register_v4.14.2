/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.PageObjects.FR1_RiskRegister_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.PageObjects.FR2_FR10_RiskRegister_PageObjects;
import org.openqa.selenium.JavascriptExecutor;


/**
 *
 * @author Vijaya
 */
@KeywordAnnotation(
        Keyword = "UC_RSM_06_02_Capture_MitigationControls_MainScenario",
        createNewBrowserInstance = false
)
public class UC_RSM_06_02_Capture_MitigationControls_MainScenario extends BaseClass {

    String error = "";

    public UC_RSM_06_02_Capture_MitigationControls_MainScenario() {

    }

    public TestResult executeTest() {

        if (!addMitigationControls()) {
            return narrator.testFailed("Failed to add Mitigation Controls- " + error);
        }
        
        return narrator.finalizeTest("Successfully added Mitigation Controls");
    }

    public boolean addMitigationControls() {
        
        //Mitigation Controls Add button
         if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.mitigationControlsAddButton(), 5000)){
            error = "Failed to wait for Mitigation Controls Add button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.mitigationControlsAddButton())){
            error = "Failed to click on Mitigation Controls Add button";
            return false;
        }
       
        pause(5000);
        //Mitigation Controls processflow
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.mitigationControlsProcessflow())){
            error = "Failed to wait for Mitigation Controls Processflow";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.mitigationControlsProcessflow())){
            error = "Failed to click on Mitigation Controls Processflow";
            return false;
        }
        
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Processflow in Add phase");
        String processStatusAdd = SeleniumDriverInstance.retrieveAttributeByXpath(FR1_RiskRegister_PageObjects.processFlowStatusChild("Add phase"),"class");
        if (!processStatusAdd.equalsIgnoreCase("step active")) {
        error = "Failed to be in Add phase";
        return false;
        }
        
        
        //Control category
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.controlCategoryDropdown())){
            error = "Failed to wait for Control category dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.controlCategoryDropdown())){
            error = "Failed to click Control category dropdown";
            return false;
        }
        
        pause(3000);
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.controlCategoryDropdownExpand(testData.getData("Control category parent value"),"1"))) {
            error = "Failed to wait for Control category dropdown Expand";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.controlCategoryDropdownExpand(testData.getData("Control category parent value"),"1"))) {
            error = "Failed to click Control category dropdown Expand";
            return false;
        }
            
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Control category")))) {
            error = "Failed to wait for Control category dropdown value";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Control category")))) {
            error = "Failed to click on Control category dropdown value";
            return false;
        }
        
        
        //Control Description
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.controlDescription()))
        {
        error = "Failed to locate Control Description field";
        return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(FR2_FR10_RiskRegister_PageObjects.controlDescription(),testData.getData("Control Description")))
        {
        error = "Failed to enter text in Control Description field";
        return false;
        }
        
        //Control owner dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.controlOwnerDropdown())){
            error = "Failed to wait for Control owner dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.controlOwnerDropdown())){
            error = "Failed to click Control owner dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Control owner dropdown value")))) {
            error = "Failed to wait for Control owner dropdown value";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Control owner dropdown value")))) {
            error = "Failed to click on Control owner dropdown value";
            return false;
        }
        
        //Group/Site dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.groupSiteDropdown())){
            error = "Failed to wait for Group/Site dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.groupSiteDropdown())){
            error = "Failed to click Group/Site dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Group or Site dropdown value")))) {
            error = "Failed to wait for Group/Site dropdown value";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Group or Site dropdown value")))) {
            error = "Failed to click on Group/Site dropdown value";
            return false;
        }
        
        //Critical checkbox
        pause(1000);
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.criticalCheckbox())) {
            error = "Failed to wait for Critical check box.";
            return false;
         }
           
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.criticalCheckbox())) {
                error = "Failed to click on Critical check box.";
                return false;
         }
        
        //Control implementation dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.controlImplementationDropdown())){
            error = "Failed to wait for Control implementation dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.controlImplementationDropdown())){
            error = "Failed to click Control implementation dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Control implementation dropdown value")))) {
            error = "Failed to wait for Control implementation dropdown value";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Control implementation dropdown value")))) {
            error = "Failed to click on Control implementation dropdown value";
            return false;
        }
        
        //Control quality dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.controlQualityDropdown())){
            error = "Failed to wait for Control quality dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.controlQualityDropdown())){
            error = "Failed to click Control quality dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Control quality dropdown value")))) {
            error = "Failed to wait for Control quality dropdown value";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Control quality dropdown value")))) {
            error = "Failed to click on Control quality dropdown value";
            return false;
        }
        
        //Monitoring frequency dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.monitoringFrequencyDropdown())){
            error = "Failed to wait for Monitoring frequency dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.monitoringFrequencyDropdown())){
            error = "Failed to click Monitoring frequency dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Monitoring frequency dropdown value")))) {
            error = "Failed to wait for Monitoring frequency dropdown value";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Monitoring frequency dropdown value")))) {
            error = "Failed to click on Monitoring frequency dropdown value";
            return false;
        }
        
        //Monitoring owner dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.monitoringOwnerDropdown())){
            error = "Failed to wait for Monitoring owner dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.monitoringOwnerDropdown())){
            error = "Failed to click Monitoring owner dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Monitoring owner dropdown value")))) {
            error = "Failed to wait for Monitoring owner dropdown value";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Monitoring owner dropdown value")))) {
            error = "Failed to click on Monitoring owner dropdown value";
            return false;
        }
        
        
        //Mitigation Controls Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.mitigationControlsSaveButton())) {
            error = "Failed to wait for Mitigation Controls Save button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.mitigationControlsSaveButton())) {
            error = "Failed to click on Mitigation Controls Save button.";
            return false;
        }

        pause(15000);
        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(FR2_FR10_RiskRegister_PageObjects.mitigationControlsRecordNumber()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());
        
        narrator.stepPassedWithScreenShot("Processflow moves to Edit phase");
        String processStatusEdit = SeleniumDriverInstance.retrieveAttributeByXpath(FR1_RiskRegister_PageObjects.processFlowStatusChild("Edit phase"),"class");
        if (!processStatusEdit.equalsIgnoreCase("step active")) {
        error = "Failed to be in Edit phase";
        return false;
        }
        
        return true;
    }

}
