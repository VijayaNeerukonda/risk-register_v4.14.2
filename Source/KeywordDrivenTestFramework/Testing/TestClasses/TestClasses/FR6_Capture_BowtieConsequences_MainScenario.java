/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.PageObjects.FR1_RiskRegister_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.PageObjects.FR2_FR10_RiskRegister_PageObjects;


/**
 *
 * @author Vijaya
 */
@KeywordAnnotation(
        Keyword = "FR6_Capture_BowtieConsequences_MainScenario",
        createNewBrowserInstance = false
)
public class FR6_Capture_BowtieConsequences_MainScenario extends BaseClass {

    String error = "";

    public FR6_Capture_BowtieConsequences_MainScenario() {

    }

    public TestResult executeTest() {

        if (!addConsequences()) {
            return narrator.testFailed("Failed to add Consequences - " + error);
        }
        
        return narrator.finalizeTest("Successfully added Consequences");
    }

    public boolean addConsequences() {
        //Consequences panel
         if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.consequencesPanel(), 5000)){
            error = "Failed to wait for Consequences panel";
            return false;
        }
         
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.consequencesPanel())){
            error = "Failed to click on Consequences panel";
            return false;
        }
        
        //Consequences Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.consequencesAddButton())){
            error = "Failed to wait for Consequences Add button";
            return false;
        }
         
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.consequencesAddButton())){
            error = "Failed to click on Consequences Add button";
            return false;
        }
        
        SeleniumDriverInstance.pause(6000);
         //Processflow
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.consequencesProcessflow())){
            error = "Failed to wait for Consequences Processflow";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.consequencesProcessflow())){
            error = "Failed to click on Consequences Processflow";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Processflow in Add phase");
        String processStatusAdd = SeleniumDriverInstance.retrieveAttributeByXpath(FR1_RiskRegister_PageObjects.processFlowStatus("Add phase"),"class");
        if (!processStatusAdd.equalsIgnoreCase("step active")) {
        error = "Failed to be in Add phase";
        return false;
        }
        
        //Consequence type dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.consequenceTypeDropdown())){
            error = "Failed to wait for Consequence type dropdown";
            return false;
        }
         
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.consequenceTypeDropdown())){
            error = "Failed to click Consequence type dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Consequence type dropdown value")))) {
            error = "Failed to wait for Consequence type dropdown value";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Consequence type dropdown value")))) {
            error = "Failed to click on Consequence type dropdown value";
            return false;
        }
        
        //Maximum Consequence dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.maxConsequenceDropdown())){
            error = "Failed to wait for Maximum Consequence dropdown";
            return false;
        }
         
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.maxConsequenceDropdown())){
            error = "Failed to click Maximum Consequence dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.controlsDropdownExpand(testData.getData("Maximum Consequence Expand"),"1"))) {
            error = "Failed to wait for Maximum Consequence dropdown Expand";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.controlsDropdownExpand(testData.getData("Maximum Consequence Expand"),"1"))) {
            error = "Failed to click Maximum Consequence dropdown Expand";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Maximum Consequence child value")))) {
            error = "Failed to wait for Maximum Consequence dropdown child value";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Maximum Consequence child value")))) {
            error = "Failed to click on Maximum Consequence dropdown child value";
            return false;
        }
        
        //Consequence dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.consequenceDropdown())){
            error = "Failed to wait for Consequence dropdown";
            return false;
        }
         
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.consequenceDropdown())){
            error = "Failed to click Maximum dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.controlsDropdownExpand(testData.getData("Consequence Expand"),"1"))) {
            error = "Failed to wait for Consequence dropdown Expand";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.controlsDropdownExpand(testData.getData("Consequence Expand"),"1"))) {
            error = "Failed to click Consequence dropdown Expand";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Consequence child value")))) {
            error = "Failed to wait for Consequence dropdown child value";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Consequence child value")))) {
            error = "Failed to click on Consequence dropdown child value";
            return false;
        }
        
        //Consequences textbox
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.consequencesTextbox())) {
            error = "Failed to wait for Consequences textbox";
            return false;
        }
        
        if (!SeleniumDriverInstance.enterTextByXpath(FR2_FR10_RiskRegister_PageObjects.consequencesTextbox(), getData("Consequences Textbox"))) {
            error = "Failed to enter text in Consequences textbox";
            return false;
        } 
        
                       
        //Controls dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.consqControlsDropdown())){
            error = "Failed to wait for Controls dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.consqControlsDropdown())){
            error = "Failed to click Controls dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.controlsDropdownExpand(testData.getData("Controls parent value"),"1"))) {
            error = "Failed to wait for Controls dropdown Expand";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.controlsDropdownExpand(testData.getData("Controls parent value"),"1"))) {
            error = "Failed to click Controls dropdown Expand";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.checklistSelect(getData("Controls child value")))) {
            error = "Failed to wait for Controls child value";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.checklistSelect(getData("Controls child value")))) {
            error = "Failed to click on Controls child value";
            return false;
        }
        
        //Consequences Save button
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.consequencesSaveButton())){
            error = "Failed to wait for Consequences Save button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.consequencesSaveButton())){
            error = "Failed to click Consequences Save button";
            return false;
        }
        
       if (!SeleniumDriverInstance.WaitForLoaderToDisappear(FR1_RiskRegister_PageObjects.saveWait())) {
            error = "Failed to wait for 'Save wait' button.";
            return false;
        }

       
        //Validate save
        if (!SeleniumDriverInstance.waitForElementsByXpath(FR1_RiskRegister_PageObjects.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(FR1_RiskRegister_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");

        
        narrator.stepPassedWithScreenShot("Successfully saved the record and processflow moved to Editphase");
        String processStatusEdit = SeleniumDriverInstance.retrieveAttributeByXpath(FR1_RiskRegister_PageObjects.processFlowStatus("Edit phase"),"class");
        if (!processStatusEdit.equalsIgnoreCase("step active")) {
        error = "Failed to be in Edit phase";
        return false;
        }
        
        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(FR2_FR10_RiskRegister_PageObjects.consequencesRecordNumber()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());
        
        pause(5000);
        //Replication to Mitigation controls
         if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.mitigationControlsReplication())){
            error = "Failed to wait for Mitigation controls replicated records";
            return false;
        }
         
        return true;
    }

}
