/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.PageObjects.FR1_RiskRegister_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.PageObjects.FR2_FR10_RiskRegister_PageObjects;


/**
 *
 * @author Vijaya
 */
@KeywordAnnotation(
        Keyword = "FR13_Capture_IntegratedRiskActions_MainScenario",
        createNewBrowserInstance = false
)
public class FR13_Capture_IntegratedRiskActions_MainScenario extends BaseClass {

    String error = "";

    public FR13_Capture_IntegratedRiskActions_MainScenario() {

    }

    public TestResult executeTest() {

        if (!addIntegratedRiskActions()) {
            return narrator.testFailed("Failed to add Integrated Risk Actions - " + error);
        }
        
        return narrator.finalizeTest("Successfully added Integrated Risk Actions");
    }

    public boolean addIntegratedRiskActions() {
        
        //Actions panel
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.actionsPanel(), 5000)){
            error = "Failed to wait for Actions panel";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.actionsPanel())){
            error = "Failed to click on Actions panel";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Integrated Risk Actions non-editable grid is displayed");
         
        //Integrated Risk Actions Add button
         if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.integratedRiskActionsAddButton(), 5000)){
            error = "Failed to wait for Integrated Risk Actions Add button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.integratedRiskActionsAddButton())){
            error = "Failed to click on Integrated Risk Actions Add button";
            return false;
        }
        
        pause(40000);
        //Integrated Risk Actions processflow button
         if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.integratedRiskActionsProcessflowButton(), 5000)){
            error = "Failed to wait for Integrated Risk Actions processflow button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.integratedRiskActionsProcessflowButton())){
            error = "Failed to click on Integrated Risk Actions processflow button";
            return false;
        }
        narrator.stepPassedWithScreenShot("integrated Risk Actions opens in Add action phase");
        String processStatus2 = SeleniumDriverInstance.retrieveAttributeByXpath(FR1_RiskRegister_PageObjects.processFlowStatus("Add action"),"class");
        if (!processStatus2.equalsIgnoreCase("step active")) {
        error = "Failed to be in Add action phase";
        return false;
        }
        
        //Action Description
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.actionDescription()))
        {
        error = "Failed to locate Description field";
        return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(FR2_FR10_RiskRegister_PageObjects.actionDescription(),testData.getData("Action Description")))
        {
        error = "Failed to enter text in Description field";
        return false;
        }
        
        //Department Responsible
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.departmentResponsible())){
            error = "Failed to wait for Department Responsible dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.departmentResponsible())){
            error = "Failed to click Department Responsible dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Department Responsible")))) {
            error = "Failed to wait for Department Responsible dropdown value";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Department Responsible")))) {
            error = "Failed to click on Department Responsible dropdown value";
            return false;
        }
        
        
        //Responsible person
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.responsiblePerson())){
            error = "Failed to wait for Responsible person dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.responsiblePerson())){
            error = "Failed to click Responsible person dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.singleSelectIndex(getData("Responsible person"),"1"))) {
            error = "Failed to wait for Responsible person dropdown value";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.singleSelectIndex(getData("Responsible person"),"1"))) {
            error = "Failed to click on Responsible person dropdown value";
            return false;
        }
        
        //Action due date
        String actionduedate=SeleniumDriverInstance.getADate("", 0);

        if (!SeleniumDriverInstance.enterTextByXpath(FR2_FR10_RiskRegister_PageObjects.actiondueDateXpath(), actionduedate))
        {
        error = "Failed to enter Action due date";
        return false;
        }
        
        //Integrated Risk Actions Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.integratedRiskActionsSaveButton())) {
            error = "Failed to wait for Integrated Risk Actions Save button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.integratedRiskActionsSaveButton())) {
            error = "Failed to click on Integrated Risk Actions Save button";
            return false;
        }

        if (!SeleniumDriverInstance.WaitForLoaderToDisappear(FR1_RiskRegister_PageObjects.saveWait())) {
            error = "Failed to wait for Saving…";
            return false;
        }

        //Validate Save
        if (!SeleniumDriverInstance.waitForElementsByXpath(FR1_RiskRegister_PageObjects.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(FR1_RiskRegister_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");


        narrator.stepPassedWithScreenShot("Sucessfully saved the record and processflow moves to To be initiated phase");
        String processStatus3 = SeleniumDriverInstance.retrieveAttributeByXpath(FR1_RiskRegister_PageObjects.processFlowStatus("To be initiated"),"class");
        if (!processStatus3.equalsIgnoreCase("step active")) {
        error = "Failed to move to To be initiated phase";
        return false;
        }
        
        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(FR2_FR10_RiskRegister_PageObjects.integratedRiskActionsRecordNumber()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());
        
        return true;
    }

}
