/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.PageObjects.FR1_RiskRegister_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.PageObjects.FR2_FR10_RiskRegister_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import static org.apache.log4j.NDC.clear;
import org.openqa.selenium.By;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR7_ViewUpdatePrint_BowtieView_AlternateScenario2",
        createNewBrowserInstance = false
)

public class FR7_ViewUpdatePrint_BowtieView_AlternateScenario2 extends BaseClass {

    String parentWindow;
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR7_ViewUpdatePrint_BowtieView_AlternateScenario2() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!View_Bowtie()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully viewed and printed bowtie view");
    }

    public boolean View_Bowtie() {
        //Navigate to Intergrated Risk Register
        if(!SeleniumDriverInstance.waitForElementByXpath(FR1_RiskRegister_PageObjects.navigate_IntegratedRiskRegister())){
            error = "Failed to wait for 'Integrated Risk Register'";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR1_RiskRegister_PageObjects.navigate_IntegratedRiskRegister())){
            error = "Failed to click on 'Integrated Risk Register'";
            return false;
        }
        SeleniumDriverInstance.pause(5000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Integrated Risk Register' tab.");
       
          //Search Button
        pause(5000);
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.searchButtonOnSearchpage()))
        {
        error = "Failed to locate Search Button";
        return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.searchButtonOnSearchpage()))
        {
        error = "Failed to click Search Button";
        return false;
        }

        pause(10000);
        narrator.stepPassedWithScreenShot("Successfully showing the list of all Risk Register records");
        
        //Open risk record
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.openRiskRecord()))
        {
        error = "Failed to wait to Open risk record";
        return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.openRiskRecord()))
        {
        error = "Failed to Open risk record";
        return false;
        }

        pause(5000);
        //Open risk assessment record
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.openRiskAssessmentRecord()))
        {
        error = "Failed to wait to Open risk assessment record";
        return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.openRiskAssessmentRecord()))
        {
        error = "Failed to Open risk assessment record";
        return false;
        }
        
        pause(15000);
        //View bowtie
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.View_Bowtie())) {
            error = "Failed to wait for 'View bowtie' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.View_Bowtie())) {
            error = "Failed to click on 'View bowtie' button.";
            return false;
        }
        pause(15000);
        narrator.stepPassedWithScreenShot("Successfully clicked 'View bowtie' button.");

        String BowtieAssessments = getData("Bowtie Assessments");
        switch (BowtieAssessments) {

            case "Add Consequences":
                //Consequence block
                 //+ icon (Add)
                if (!SeleniumDriverInstance.moveToElementByXpath(FR2_FR10_RiskRegister_PageObjects.moveToElement())) {
                    error = "Failed to move to + icon on right side";
                    return false;
                }

                 if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.addConsequencePlusIcon())) {
                    error = "Failed to wait for + icon on right side to open Consequence block";
                    return false;
                }

                if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.addConsequencePlusIcon())) {
                    error = "Failed to click on the + icon on right side to open Consequence block";
                    return false;
                }
        
                //Consequence block - Consequence type dropdown
                if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.diagramConsequenceTypeDropdown())) {
                    error = "Failed to wait for Consequence block - Consequence type dropdown";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.diagramConsequenceTypeDropdown())) {
                    error = "Failed to click on Consequence block - Consequence type dropdown";
                    return false;
                }
                
                if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.singleSelectIndex(getData("Consequence type dropdown value"),"1"))) {
                error = "Failed to wait for Consequence type dropdown value";
                return false;
                }
                
                if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.singleSelectIndex(getData("Consequence type dropdown value"),"1"))) {
                    error = "Failed to click on Consequence type dropdown value";
                    return false;
                }
                
                 //Click to edit: Consequences text
                if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.clickToEditTextfield(("12")))){
                    error = "Failed to wait for Consequences textfield";
                    return false;
                }

                if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.clickToEditTextfield(("12")))){
                    error = "Failed to click Consequences textfield";
                    return false;
                }

                SeleniumDriverInstance.Driver.findElement(By.xpath(FR2_FR10_RiskRegister_PageObjects.clickToEditTextfield(("12")))).clear();
                if (!SeleniumDriverInstance.enterTextByXpath(FR2_FR10_RiskRegister_PageObjects.clickToEditTextfield("12"),testData.getData("Consequences text"))) {
                    error = "Failed to enter text in Consequences textfield";
                    return false;
                }
                
                //Consequence block - Maximum Consequences dropdown
                if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.diagramMaxConsequencesDropdown())) {
                    error = "Failed to wait for Consequence block - Maximum Consequences dropdown";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.diagramMaxConsequencesDropdown())) {
                    error = "Failed to click on Consequence block - Maximum Consequences dropdown";
                    return false;
                }
                
                if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.singleSelectIndex(getData("Maximum Consequences dropdown value"),"1"))) {
                error = "Failed to wait for Maximum Consequences dropdown value";
                return false;
                }
                
                if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.singleSelectIndex(getData("Maximum Consequences dropdown value"),"1"))) {
                    error = "Failed to click on Maximum Consequences dropdown value";
                    return false;
                }
                
                //Complete all Consequences and Causes - Click to edit
                String[] Causes_Text = getData("Causes Text").split("\\|");
                String[] Causes_rowNumbers = Causes_Text[0].split(",");
                String[] Causes_row_data = Causes_Text[1].split(",");
                for (int row = 0; row< Causes_rowNumbers.length; row++){
                if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.clickToEditTextfield(Causes_rowNumbers[row])))
                {
                    error = "Failed to click Causes field: " + Causes_rowNumbers[row];
                    return false;
                }

                if (!SeleniumDriverInstance.enterTextByXpath(FR2_FR10_RiskRegister_PageObjects.clickToEditTextfield(Causes_rowNumbers[row])+"//input",Causes_row_data[row]))
                {
                    error = "Failed to enter text in Causes field: " + Causes_rowNumbers[row];
                    return false;
                }
                }
                
//                String[] clickToEditTextField = getData("Click to edit").split(",");
//                String saved = "";
//
//                for(int i = 0; i < clickToEditTextField.length; i++){
//                 if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.clickToEditTextfield(clickToEditTextField[i]))){
//                error = "Failed to wait for '" + clickToEditTextField[i] + "' record.";
//                return false;
//                }
//                if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.clickToEditTextfield(clickToEditTextField[i]))){
//                    error = "Failed to click on '" + clickToEditTextField[i] + "' record.";
//                    return false;
//                }
//                
//                if (!SeleniumDriverInstance.enterTextByXpath(FR2_FR10_RiskRegister_PageObjects.clickToEditTextfield(clickToEditTextField[i]))){
//                    error = "Failed to enter text in '" + clickToEditTextField[i] + "' textfield";
//                    return false;
//                }
//                }
                
//                //Save button
//                if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.diagramSaveButton())) {
//                    error = "Failed to wait for Save button";
//                    return false;
//                }
//                if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.diagramSaveButton())) {
//                    error = "Failed to click on Save button";
//                    return false;
//                }
                
                
        }

        return true;
    }


}