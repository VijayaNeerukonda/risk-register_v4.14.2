/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.PageObjects.FR1_RiskRegister_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.PageObjects.FR2_FR10_RiskRegister_PageObjects;


/**
 *
 * @author Vijaya
 */
@KeywordAnnotation(
        Keyword = "FR10_Update_ResidualRisk_AlternateScenario1",
        createNewBrowserInstance = false
)
public class FR10_Update_ResidualRisk_AlternateScenario1 extends BaseClass {

    String error = "";

    public FR10_Update_ResidualRisk_AlternateScenario1() {

    }

    public TestResult executeTest() {

        if (!addInherentRisk()) {
            return narrator.testFailed("Failed to add Inherent Risk - " + error);
        }
        
        return narrator.finalizeTest("Successfully added Inherent Risk record");
    }

    public boolean addInherentRisk() {
       
        
        //Residual Risk refresh button
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.residualRiskRefreshButton())) {
            error = "Failed to wait for Residual Risk refresh button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.residualRiskRefreshButton())) {
            error = "Failed to click on Residual Risk refresh button";
            return false;
        }
        
        pause(15000);
         //Wait for Residual risk records
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.impactRiskRecordsReplicate())) {
            error = "Failed to wait for Residual Risk records";
            return false;
        }
        narrator.stepPassedWithScreenShot("Displays Residual risk records");
        return true;
    }

}
