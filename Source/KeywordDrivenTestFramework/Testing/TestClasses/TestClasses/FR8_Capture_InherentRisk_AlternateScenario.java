/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.PageObjects.FR1_RiskRegister_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.PageObjects.FR2_FR10_RiskRegister_PageObjects;


/**
 *
 * @author Vijaya
 */
@KeywordAnnotation(
        Keyword = "FR8_Capture_InherentRisk_AlternateScenario",
        createNewBrowserInstance = false
)
public class FR8_Capture_InherentRisk_AlternateScenario extends BaseClass {

    String error = "";

    public FR8_Capture_InherentRisk_AlternateScenario() {

    }

    public TestResult executeTest() {

        if (!editInherentRisk()) {
            return narrator.testFailed("Failed to edit Inherent Risk - " + error);
        }
        
        return narrator.finalizeTest("Successfully edited Inherent Risk record");
    }

    public boolean editInherentRisk() {
        
       
        //Likelihood rating
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.likelihoodRatingInherentRisk())){
            error = "Failed to wait for Likelihood rating dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.likelihoodRatingInherentRisk())){
            error = "Failed to click Likelihood rating dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Likelihood rating updated")))) {
            error = "Failed to wait for updated Likelihood rating dropdown value";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Likelihood rating updated")))) {
            error = "Failed to click on updated Likelihood rating dropdown value";
            return false;
        }
        
         
        narrator.stepPassedWithScreenShot("Risk rating and Risk rating description fields populated with values");
        
        
        //Impact - Inherent Risk Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.impactInherentRiskSaveButton())) {
            error = "Failed to wait for Impact - Inherent Risk 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.impactInherentRiskSaveButton())) {
            error = "Failed to click on Impact - Inherent Risk 'Save' button.";
            return false;
        }

//        if (!SeleniumDriverInstance.WaitForLoaderToDisappear(FR1_RiskRegister_PageObjects.saveWait())) {
//            error = "Failed to wait for 'Save wait' button.";
//            return false;
//        }

        
//        //Validate save
//        if (!SeleniumDriverInstance.waitForElementsByXpath(FR1_RiskRegister_PageObjects.validateSave())) {
//            error = "Failed to wait for Save validation.";
//            return false;
//        }
//        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(FR1_RiskRegister_PageObjects.validateSave());
//
//        if (!SaveFloat.equals("Record saved")) {
//            narrator.stepPassedWithScreenShot("Failed to save record.");
//            return false;
//        }
//        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");

        
        pause(30000);
        narrator.stepPassedWithScreenShot("Inherent Risk on Risk Assessment updated");
        
        return true;
    }

}
