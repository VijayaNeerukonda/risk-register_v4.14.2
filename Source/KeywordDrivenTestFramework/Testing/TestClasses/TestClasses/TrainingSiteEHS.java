/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.PageObjects.TrainingSiteEHSPageObjects;

/**
 *
 * @author vijaya
 */

@KeywordAnnotation
(
    Keyword = "Click on Environment, Health & Safety",
    createNewBrowserInstance = false
)
public class TrainingSiteEHS extends BaseClass
{

    String error = "";

    public TrainingSiteEHS()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!navigateToAPageFromEHSPage())
        {
            return narrator.testFailed("Failed to navigate to a module from EHS page" + error);
        }
        return narrator.finalizeTest("Successfully Navigated to module from EHS page");
    }

   
    public boolean navigateToAPageFromEHSPage() throws InterruptedException
    {
       
        if (!SeleniumDriverInstance.waitForElementByXpath(TrainingSiteEHSPageObjects.linkForAPageInHomePageXpath(testData.getData("EHSPageName")))) {
            error = "Failed to locate the module: "+testData.getData("EHSPageName");
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(TrainingSiteEHSPageObjects.linkForAPageInHomePageXpath(testData.getData("EHSPageName"))))
        {
            error = "Failed to navigate to the module: "+testData.getData("EHSPageName");
            return false;
        }

        
        return true;

    }

}
