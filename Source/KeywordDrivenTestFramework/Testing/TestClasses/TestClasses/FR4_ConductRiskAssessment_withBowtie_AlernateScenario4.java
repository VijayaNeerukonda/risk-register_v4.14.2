/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.PageObjects.FR1_RiskRegister_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.PageObjects.FR2_FR10_RiskRegister_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.PageObjects.TrainingSiteHomePageObjects;


/**
 *
 * @author Vijaya
 */
@KeywordAnnotation(
        Keyword = "FR4_ConductRiskAssessment_withBowtie_AlernateScenario4",
        createNewBrowserInstance = false
)
public class FR4_ConductRiskAssessment_withBowtie_AlernateScenario4 extends BaseClass {

    String error = "";

    public FR4_ConductRiskAssessment_withBowtie_AlernateScenario4() {

    }

    public TestResult executeTest() {

        if (!addRiskAssessment()) {
            return narrator.testFailed("Failed to add new Risk - " + error);
        }
        
        return narrator.finalizeTest("Successfully added new Risk");
    }

    public boolean addRiskAssessment() {
        
        
       //Set the page as a parent page
        String parentWindow = SeleniumDriverInstance.Driver.getWindowHandle();


        //Risk Assessment Add button
         if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.riskAssessmentAddButton(), 5000)){
            error = "Failed to wait for Risk Assessment Add button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.riskAssessmentAddButton())){
            error = "Failed to click on Risk Assessment Add button";
            return false;
        }
        
        SeleniumDriverInstance.pause(8000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");
        
         //Processflow
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.riskAssessmentProcessflow())){
            error = "Failed to wait for 'Processflow' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.riskAssessmentProcessflow())){
            error = "Failed to click on 'Processflow' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Processflow in 1.Logging a Risk Assessment phase");
         String processStatusAdd = SeleniumDriverInstance.retrieveAttributeByXpath(FR1_RiskRegister_PageObjects.processFlowStatus("1. Logging a Risk Assessment"),"class");
        if (!processStatusAdd.equalsIgnoreCase("step active")) {
        error = "Failed to be in 1. Logging a Risk Assessment phase";
        return false;
        }
        
       
        //Risk Source dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.riskSourceDropdown())){
            error = "Failed to wait for Risk Source dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.riskSourceDropdown())){
            error = "Failed to click Risk Source dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.riskSourceDropdownExpand(testData.getData("Risk Source parent value"),"2"))) {
            error = "Failed to wait for Risk Source dropdown Expand";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.riskSourceDropdownExpand(testData.getData("Risk Source parent value"),"2"))) {
            error = "Failed to click Risk Source dropdown Expand";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Risk Source child value")))) {
            error = "Failed to wait for Risk Source child value";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Risk Source child value")))) {
            error = "Failed to click on Risk Source child value";
            return false;
        }
        
         //Risk Source Checkbox
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.riskSourceCheckbox())){
            error = "Failed to wait for Risk Source Checkbox";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.riskSourceCheckbox())){
            error = "Failed to click Risk Source Checkbox";
            return false;
        }
        
         narrator.stepPassedWithScreenShot("Risk source 1- 5 fields poupulated");
        
         
        //Risk dropdown
//        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.riskDropdown())){
//            error = "Failed to wait for Risk dropdown";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.riskDropdown())){
//            error = "Failed to click Risk dropdown";
//            return false;
//        }
        
        //Add new risk
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.addNewButton())){
            error = "Failed to wait for Add new button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.addNewButton())){
            error = "Failed to click Add new button";
            return false;
        }
        
        //switch to new window
        if(!SeleniumDriverInstance.switchToWindow()){
        error = "Failed to switch to new window or tab.";
        return false;
        }
        
        //Switch to frame
        pause(10000);
        if (!SeleniumDriverInstance.swithToFrameByName(TrainingSiteHomePageObjects.iframeName()))
        {
            error = "Failed to switch to frame ";
        }
        narrator.stepPassedWithScreenShot("Successfully switched to frame");
        
        //Risk Library processflow
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.riskLibraryProcessflow())){
            error = "Failed to wait for Risk Library processflow";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.riskLibraryProcessflow())){
            error = "Failed to click Risk Library processflow";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Processflow in Add phase");
        String processStatus2 = SeleniumDriverInstance.retrieveAttributeByXpath(FR1_RiskRegister_PageObjects.processFlowStatus("Add phase"),"class");
        if (!processStatus2.equalsIgnoreCase("step active")) {
        error = "Failed to be in Add phase";
        return false;
        }
        
        //Risk textbox
         if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.riskLibraryRisk())) {
            error = "Failed to wait for Risk textbox";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(FR2_FR10_RiskRegister_PageObjects.riskLibraryRisk(), getData("Risk Library Risk"))) {
            error = "Failed to enter text in Risk textbox";
            return false;
        } 
         
        //Risk Library Impact Type
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.riskLibraryImpactType())){
            error = "Failed to wait for Risk Library Impact Type";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.riskLibraryImpactType())){
            error = "Failed to click Risk Library Impact Type";
            return false;
        }
        
        pause(3000);
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.riskLibraryImpactTypeSelectAll())){
            error = "Failed to wait for Risk Library Impact Type select all";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.riskLibraryImpactTypeSelectAll())){
            error = "Failed to click Risk Library Impact Type select all";
            return false;
        }
        
        //Priority unwanted event?
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.riskLibraryPUEDropdown())){
            error = "Failed to wait for Priority unwanted event? dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.riskLibraryPUEDropdown())){
            error = "Failed to click Priority unwanted event? dropdown";
            return false;
        }
        
         if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Priority unwanted event")))) {
            error = "Failed to wait for Priority unwanted event? value";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Priority unwanted event")))) {
            error = "Failed to click on Priority unwanted event? value";
            return false;
        }
        
        //Related critical control inspection
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.relatedCriticalControlDropdown())){
            error = "Failed to wait for Related critical control inspection dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.relatedCriticalControlDropdown())){
            error = "Failed to click Related critical control inspection dropdown";
            return false;
        }
        
         if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Related critical control inspection")))) {
            error = "Failed to wait for Related critical control inspection value";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Related critical control inspection")))) {
            error = "Failed to click on Related critical control inspection value";
            return false;
        }
        
        //Risk Library Save button
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.riskLibrarySave())){
            error = "Failed to wait for Risk Library Save button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.riskLibrarySave())){
            error = "Failed to click Risk Library Save button";
            return false;
        }
        
        pause(15000);
        narrator.stepPassedWithScreenShot("Successfully saved record and processflow moves to Edit phase");
        String processStatus3 = SeleniumDriverInstance.retrieveAttributeByXpath(FR1_RiskRegister_PageObjects.processFlowStatus("Edit phase"),"class");
        if (!processStatus3.equalsIgnoreCase("step active")) {
        error = "Failed to move to Edit phase";
        return false;
        }
        
        //Related Risk Sources checkbox
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.expandChevronRelatedRisk(getData("Related Risk Sources parent value"),"1"))) {
            error = "Failed to wait for Related Risk Sources expand chevron";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.expandChevronRelatedRisk(getData("Related Risk Sources parent value"),"1"))) {
            error = "Failed to click on Related Risk Sources expand chevron";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.relatedRiskSourceCheckbox(getData("Related Risk Sources")))) {
            error = "Failed to wait for Related Risk value";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.relatedRiskSourceCheckbox(getData("Related Risk Sources")))) {
            error = "Failed to click on Related Risk value";
            return false;
        }
        
        //Risk Library Save button
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.riskLibrarySave())){
            error = "Failed to wait for Risk Library Save button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.riskLibrarySave())){
            error = "Failed to click Risk Library Save button";
            return false;
        }
        
         if (!SeleniumDriverInstance.WaitForLoaderToDisappear(FR1_RiskRegister_PageObjects.saveWait())) {
            error = "Failed to wait for 'Save wait' button.";
            return false;
        }

        //Validate save
        if (!SeleniumDriverInstance.waitForElementsByXpath(FR1_RiskRegister_PageObjects.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(FR1_RiskRegister_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");
  
        //Close the current window
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);
        System.out.println(SeleniumDriverInstance.Driver.getTitle());

        //switch to parent window iframe
        SeleniumDriverInstance.switchToDefaultContent();
        SeleniumDriverInstance.swithToFrameByName("ifrMain");

        //Risk Assessment close
         if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.riskAssessmentClose())){
            error = "Failed to wait for Risk Assessment close";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.riskAssessmentClose())){
            error = "Failed to click Risk Assessment close";
            return false;
        }
        
        //Risk Assessment Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.riskAssessmentAddButton(), 5000)){
            error = "Failed to wait for Risk Assessment Add button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.riskAssessmentAddButton())){
            error = "Failed to click on Risk Assessment Add button";
            return false;
        }
        
        
        //Risk Source dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.riskSourceDropdown())){
            error = "Failed to wait for Risk Source dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.riskSourceDropdown())){
            error = "Failed to click Risk Source dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.riskSourceDropdownExpand(testData.getData("Risk Source parent value"),"1"))) {
            error = "Failed to wait for Risk Source dropdown Expand";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.riskSourceDropdownExpand(testData.getData("Risk Source parent value"),"1"))) {
            error = "Failed to click Risk Source dropdown Expand";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Risk Source child value")))) {
            error = "Failed to wait for Risk Source child value";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Risk Source child value")))) {
            error = "Failed to click on Risk Source child value";
            return false;
        }
        
        //Risk Source Checkbox
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.riskSourceCheckbox())){
            error = "Failed to wait for Risk Source Checkbox";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.riskSourceCheckbox())){
            error = "Failed to click Risk Source Checkbox";
            return false;
        }
        
         narrator.stepPassedWithScreenShot("Risk source 1- 5 fields poupulated");
        
                 
         //Risk dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.riskDropdown())){
            error = "Failed to wait for Risk dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.riskDropdown())){
            error = "Failed to click Risk dropdown";
            return false;
        }
        
        
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Risk")))) {
            error = "Failed to wait for Risk value";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Risk")))) {
            error = "Failed to click on Risk value";
            return false;
        }
         
        pause(1000);
       narrator.stepPassedWithScreenShot("Priority Unwanted event checkbox selected and Bowtie reference number fields are displayed");
       
          //Risk Checkbox
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.riskCheckbox())){
            error = "Failed to wait for Risk Checkbox";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.riskCheckbox())){
            error = "Failed to click Risk Checkbox";
            return false;
        }
        
        pause(5000);
        narrator.stepPassedWithScreenShot("Risk description, Layman's terms /Causes, Penalties / Consequences fields displayed");
            
         
         //Layman's terms /Causes
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.laymansTermsCauses())) {
            error = "Failed to wait for Layman's terms /Causes";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(FR2_FR10_RiskRegister_PageObjects.laymansTermsCauses(), getData("Layman's terms /Causes"))) {
            error = "Failed to enter text in Layman's terms /Causes";
            return false;
        } 
        
        //Penalties / Consequences
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.penaltiesConsequences())) {
            error = "Failed to wait for Penalties / Consequences";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(FR2_FR10_RiskRegister_PageObjects.penaltiesConsequences(), getData("Penalties / Consequences"))) {
            error = "Failed to enter text in Penalties / Consequences";
            return false;
        } 
        
        //Activity checklist
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.activitySelectAll())){
            error = "Failed to wait for Activity checklist";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.activitySelectAll())){
            error = "Failed to click on Activity checklist selectall";
            return false;
        }
        
         //Risk Owner
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.riskOwner())){
            error = "Failed to wait for Risk Owner";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.riskOwner())){
            error = "Failed to click Risk Owner";
            return false;
        }
        
        
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Risk Owner")))) {
            error = "Failed to wait for Risk Owner value";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Risk Owner")))) {
            error = "Failed to click on Risk Owner value";
            return false;
        }
        
        
        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.saveRiskAssessmentButton())) {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.saveRiskAssessmentButton())) {
            error = "Failed to click on 'Save' button.";
            return false;
        }
        
       if (!SeleniumDriverInstance.WaitForLoaderToDisappear(FR1_RiskRegister_PageObjects.saveWait())) {
            error = "Failed to wait for 'Save wait' button.";
            return false;
        }

        SeleniumDriverInstance.pause(15000);
        //Validate save
        if (!SeleniumDriverInstance.waitForElementsByXpath(FR1_RiskRegister_PageObjects.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }
        String SaveFloat1 = SeleniumDriverInstance.retrieveTextByXpath(FR1_RiskRegister_PageObjects.validateSave());

        if (!SaveFloat1.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");

        narrator.stepPassedWithScreenShot("Successfully saved the record and processflow moved to 2. In Progress phase");
        String processStatus4 = SeleniumDriverInstance.retrieveAttributeByXpath(FR1_RiskRegister_PageObjects.processFlowStatus("2. In Progress"),"class");
        if (!processStatus4.equalsIgnoreCase("step active")) {
        error = "Failed to move to  2. In Progress phase";
        return false;
        }
        
        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(FR2_FR10_RiskRegister_PageObjects.riskAssessmentRecordNumber()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());
        
        //Additional information checkbox
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.additionalInformationCheckbox())) {
            error = "Failed to wait for Additional information checkbox.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.additionalInformationCheckbox())) {
            error = "Failed to click on Additional information checkbox";
            return false;
        }
        
        pause(2000);
        narrator.stepPassedWithScreenShot("Linked Strategic Objectives, Linked Compliance Requirements, Linked Key Risk Indicators, Related Incidents, Related Findings, Supporting Documents panels are displayed");
        
        return true;
    }

}
