/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.PageObjects.FR1_RiskRegister_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.PageObjects.FR2_FR10_RiskRegister_PageObjects;
import org.openqa.selenium.JavascriptExecutor;


/**
 *
 * @author Vijaya
 */
@KeywordAnnotation(
        Keyword = "UC_RSM_05_02_Capture_PreventativeControls_MainScenario",
        createNewBrowserInstance = false
)
public class UC_RSM_05_02_Capture_PreventativeControls_MainScenario extends BaseClass {

    String error = "";

    public UC_RSM_05_02_Capture_PreventativeControls_MainScenario() {

    }

    public TestResult executeTest() {

        if (!addPreventiveControls()) {
            return narrator.testFailed("Failed to add Preventive Controls- " + error);
        }
        
        return narrator.finalizeTest("Successfully added Preventive Controls");
    }

    public boolean addPreventiveControls() {
        
        //Preventive Controls Add button
         if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.preventiveControlsAddButton(), 5000)){
            error = "Failed to wait for Preventive Controls Add button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.preventiveControlsAddButton())){
            error = "Failed to click on Preventive Controls Add button";
            return false;
        }
       
        pause(20000);
        //Preventive Controls processflow
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.preventiveControlsProcessflow())){
            error = "Failed to wait for Preventive Controls Processflow";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.preventiveControlsProcessflow())){
            error = "Failed to click on Preventive Controls Processflow";
            return false;
        }
        
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Processflow in Add phase");
        String processStatusAdd = SeleniumDriverInstance.retrieveAttributeByXpath(FR1_RiskRegister_PageObjects.processFlowStatusChild("Add phase"),"class");
        if (!processStatusAdd.equalsIgnoreCase("step active")) {
        error = "Failed to be in Add phase";
        return false;
        }
        
        
        //Control category
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.controlCategoryDropdown())){
            error = "Failed to wait for Control category dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.controlCategoryDropdown())){
            error = "Failed to click Control category dropdown";
            return false;
        }
        
        pause(3000);
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.controlCategoryDropdownExpand(testData.getData("Control category parent value"),"1"))) {
            error = "Failed to wait for Control category dropdown Expand";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.controlCategoryDropdownExpand(testData.getData("Control category parent value"),"1"))) {
            error = "Failed to click Control category dropdown Expand";
            return false;
        }
            
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Control category")))) {
            error = "Failed to wait for Control category dropdown value";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Control category")))) {
            error = "Failed to click on Control category dropdown value";
            return false;
        }
        
        
        //Control Description
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.controlDescription()))
        {
        error = "Failed to locate Control Description field";
        return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(FR2_FR10_RiskRegister_PageObjects.controlDescription(),testData.getData("Control Description")))
        {
        error = "Failed to enter text in Control Description field";
        return false;
        }
        
        //Control owner dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.controlOwnerDropdown())){
            error = "Failed to wait for Control owner dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.controlOwnerDropdown())){
            error = "Failed to click Control owner dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Control owner dropdown value")))) {
            error = "Failed to wait for Control owner dropdown value";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Control owner dropdown value")))) {
            error = "Failed to click on Control owner dropdown value";
            return false;
        }
        
        //Group/Site dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.groupSiteDropdown())){
            error = "Failed to wait for Group/Site dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.groupSiteDropdown())){
            error = "Failed to click Group/Site dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Group or Site dropdown value")))) {
            error = "Failed to wait for Group/Site dropdown value";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Group or Site dropdown value")))) {
            error = "Failed to click on Group/Site dropdown value";
            return false;
        }
        
        //Critical checkbox
        pause(1000);
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.criticalCheckbox())) {
            error = "Failed to wait for Critical check box.";
            return false;
         }
           
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.criticalCheckbox())) {
                error = "Failed to click on Critical check box.";
                return false;
         }
        
        //Control implementation dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.controlImplementationDropdown())){
            error = "Failed to wait for Control implementation dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.controlImplementationDropdown())){
            error = "Failed to click Control implementation dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Control implementation dropdown value")))) {
            error = "Failed to wait for Control implementation dropdown value";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Control implementation dropdown value")))) {
            error = "Failed to click on Control implementation dropdown value";
            return false;
        }
        
        //Control quality dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.controlQualityDropdown())){
            error = "Failed to wait for Control quality dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.controlQualityDropdown())){
            error = "Failed to click Control quality dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Control quality dropdown value")))) {
            error = "Failed to wait for Control quality dropdown value";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Control quality dropdown value")))) {
            error = "Failed to click on Control quality dropdown value";
            return false;
        }
        
        //Monitoring frequency dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.monitoringFrequencyDropdown())){
            error = "Failed to wait for Monitoring frequency dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.monitoringFrequencyDropdown())){
            error = "Failed to click Monitoring frequency dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Monitoring frequency dropdown value")))) {
            error = "Failed to wait for Monitoring frequency dropdown value";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Monitoring frequency dropdown value")))) {
            error = "Failed to click on Monitoring frequency dropdown value";
            return false;
        }
        
        //Monitoring owner dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.monitoringOwnerDropdown())){
            error = "Failed to wait for Monitoring owner dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.monitoringOwnerDropdown())){
            error = "Failed to click Monitoring owner dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Monitoring owner dropdown value")))) {
            error = "Failed to wait for Monitoring owner dropdown value";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Monitoring owner dropdown value")))) {
            error = "Failed to click on Monitoring owner dropdown value";
            return false;
        }
        
        
        //Preventive Controls Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.preventiveControlsSaveButton())) {
            error = "Failed to wait for Preventive Controls Save button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.preventiveControlsSaveButton())) {
            error = "Failed to click on Preventive Controls Save button.";
            return false;
        }

        if (!SeleniumDriverInstance.WaitForLoaderToDisappear(FR1_RiskRegister_PageObjects.saveWait())) {
            error = "Failed to wait for 'Save wait' button.";
            return false;
        }

       
        //Validate save
        if (!SeleniumDriverInstance.waitForElementsByXpath(FR1_RiskRegister_PageObjects.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(FR1_RiskRegister_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");

        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(FR2_FR10_RiskRegister_PageObjects.preventiveControlsRecordNumber()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());
        
        narrator.stepPassedWithScreenShot("Processflow moves to Edit phase");
        String processStatusEdit = SeleniumDriverInstance.retrieveAttributeByXpath(FR1_RiskRegister_PageObjects.processFlowStatusChild("Edit phase"),"class");
        if (!processStatusEdit.equalsIgnoreCase("step active")) {
        error = "Failed to be in Edit phase";
        return false;
        }
        
        return true;
    }

}
