/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.PageObjects.FR1_RiskRegister_PageObjects;


/**
 *
 * @author Vijaya
 */
@KeywordAnnotation(
        Keyword = "FR1_Capture_RiskRegister_AlternateScenario4",
        createNewBrowserInstance = false
)
public class FR1_Capture_RiskRegister_AlternateScenario4 extends BaseClass {

    String error = "";

    public FR1_Capture_RiskRegister_AlternateScenario4() {

    }

    public TestResult executeTest() {

        if (!reasonForRequest()) {
            return narrator.testFailed("Failed to Register a reason for request - " + error);
        }
        if (!projectLink()) {
            return narrator.testFailed("Failed to Check project link - " + error);
        }
        if (!businessUnit()) {
            return narrator.testFailed("Failed to Register a business unit - " + error);
        }
        if (!impactType()) {
            return narrator.testFailed("Failed to Register a impact type - " + error);
        }
        if (!registerTitleAndScope()) {
            return narrator.testFailed("Failed to Register a title and scope - " + error);
        }
        
        if (!processesToAssess()) {
            return narrator.testFailed("Failed to Register a process to be assessed - " + error);
        }
        if (!riskAssessments()) {
            return narrator.testFailed("Failed to Register a risk assessments - " + error);
        }

        return narrator.finalizeTest("Successfully Registered A Risk");
    }

    public boolean reasonForRequest() {
        //Navigate to Intergrated Risk Register
        if(!SeleniumDriverInstance.waitForElementByXpath(FR1_RiskRegister_PageObjects.navigate_IntegratedRiskRegister())){
            error = "Failed to wait for 'Integrated Risk Register'";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR1_RiskRegister_PageObjects.navigate_IntegratedRiskRegister())){
            error = "Failed to click on 'Integrated Risk Register'";
            return false;
        }
        SeleniumDriverInstance.pause(5000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Integrated Risk Register' tab.");
       
       
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(FR1_RiskRegister_PageObjects.RegisterRisk_button_Add(), 5000)){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR1_RiskRegister_PageObjects.RegisterRisk_button_Add())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        SeleniumDriverInstance.pause(8000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");
        
        //Processflow
        if(!SeleniumDriverInstance.waitForElementByXpath(FR1_RiskRegister_PageObjects.RegisterRisk_button_Processflow())){
            error = "Failed to wait for 'Processflow' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR1_RiskRegister_PageObjects.RegisterRisk_button_Processflow())){
            error = "Failed to click on 'Processflow' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Processflow in 1.Logging Risk Register phase. Risk Register Scope and Supporting Documents tabs are visible. Reason for request is defaulted to 'New Risk'");
        String retrieveReasonForReqText = SeleniumDriverInstance.retrieveTextByXpath(FR1_RiskRegister_PageObjects.reasonForRequestXpath()); 
        if (!retrieveReasonForReqText.equalsIgnoreCase("New risk")) {
                error = "Failed to retrieve Reason for request dropdown text ";
                return false;
            }

        String processStatusAdd = SeleniumDriverInstance.retrieveAttributeByXpath(FR1_RiskRegister_PageObjects.processFlowStatus("1. Logging Risk Register"),"class");
        if (!processStatusAdd.equalsIgnoreCase("step active")) {
        error = "Failed to be in 1. Logging Risk Register phase";
        return false;
        }


        
        //Reason for request
        if (!SeleniumDriverInstance.waitForElementByXpath(FR1_RiskRegister_PageObjects.reasonForRequestXpath())) {
            error = "Failed to wait for 'reason for request' field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR1_RiskRegister_PageObjects.reasonForRequestXpath())) {
            error = "Failed to click on 'reason for request' field.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(FR1_RiskRegister_PageObjects.anyReasonForRequest(getData("Request Reason")))) {
            error = "Failed to wait for '" + getData("Request Reason") + "' list.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR1_RiskRegister_PageObjects.anyReasonForRequest(getData("Request Reason")))) {
            error = "Failed to click on '" + getData("Request Reason") + "' list.";
            return false;
        }
        narrator.stepPassed("Request reason : " + getData("Request Reason") + "'.");
        narrator.stepPassedWithScreenShot("Successfully entered.");

        return true;
    }

    public boolean projectLink() {
        //Project link Checkbox
        if (getData("Project Link").equalsIgnoreCase("TRUE")) {
            //Check project link Checkbox
            if (!SeleniumDriverInstance.waitForElementByXpath(FR1_RiskRegister_PageObjects.projectLink())) {
                error = "Failed to wait for 'Project link' check box.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(FR1_RiskRegister_PageObjects.projectLink())) {
                error = "Failed to click on 'Project link' check box.";
                return false;
            }

            //Project Dropdown
            if (!SeleniumDriverInstance.waitForElementByXpath(FR1_RiskRegister_PageObjects.projectDropdown())) {
                error = "Failed to wait for 'Project' Dropdown.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(FR1_RiskRegister_PageObjects.projectDropdown())) {
                error = "Failed to click on 'Project' Dropdown.";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(FR1_RiskRegister_PageObjects.anyProject(getData("Project")))) {
                error = "Failed to wait for '" + getData("Project") + "' list.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(FR1_RiskRegister_PageObjects.anyProject(getData("Project")))) {
                error = "Failed to click on '" + getData("Project") + "' list.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Project : '" + getData("Project") + "'.");

            //Related stakeholder
            if (!SeleniumDriverInstance.waitForElementByXpath(FR1_RiskRegister_PageObjects.relatedStakeholder())) {
                error = "Failed to wait for 'Related stakeholder' Dropdown.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(FR1_RiskRegister_PageObjects.relatedStakeholder())) {
                error = "Failed to click on 'Related stakeholder' Dropdown.";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(FR1_RiskRegister_PageObjects.anyReasonForRequest(getData("Related Stakeholder")))) {
                error = "Failed to wait for '" + getData("Related Stakeholder") + "' list.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(FR1_RiskRegister_PageObjects.anyReasonForRequest(getData("Related Stakeholder")))) {
                error = "Failed to click on '" + getData("Related Stakeholder") + "' list.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Related Stakeholder : '" + getData("Related Stakeholder") + "'.");
            narrator.stepPassedWithScreenShot("Successfully Ticked.");
        } else {
            narrator.stepPassedWithScreenShot("Successfully didn't Tick.");
        }
        return true;
    }

    public boolean businessUnit() {
        //Business tab
        if (!SeleniumDriverInstance.waitForElementByXpath(FR1_RiskRegister_PageObjects.businessUnitDropdown())) {
            error = "Failed to wait for 'Business Unit' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR1_RiskRegister_PageObjects.businessUnitDropdown())) {
            error = "Failed to click on 'Business Unit' tab.";
            return false;
        }
        
        //Business Unit select
            if(!SeleniumDriverInstance.waitForElementByXpath(FR1_RiskRegister_PageObjects.businessUnitexpand("Global Company"))){
                error = "Failed to wait to expand 'Global Company'.";
                return false;
            }
            if(!SeleniumDriverInstance.clickElementbyXpath(FR1_RiskRegister_PageObjects.businessUnitexpand("Global Company"))){
                error = "Failed to expand 'Global Company'.";
                return false;
            }
            
            if(!SeleniumDriverInstance.waitForElementByXpath(FR1_RiskRegister_PageObjects.businessUnitexpand("South Africa"))){
                    error = "Failed to wait to expand 'South Africa'.";
                    return false;
                }
            
            if(!SeleniumDriverInstance.clickElementbyXpath(FR1_RiskRegister_PageObjects.businessUnitexpand("South Africa"))){
                    error = "Failed to expand 'South Africa'.";
                    return false;
                }
               
           if (!SeleniumDriverInstance.clickElementbyXpath(FR1_RiskRegister_PageObjects.businessUnitValue(getData("Business Unit")))) {
            error = "Failed to click on Business unit value";
        }
           
           return true;
    }

    public boolean registerTitleAndScope() {
        //Risk register title
        if (!SeleniumDriverInstance.waitForElementByXpath(FR1_RiskRegister_PageObjects.riskRegisterTitle())) {
            error = "Failed to wait for 'Risk register title' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(FR1_RiskRegister_PageObjects.riskRegisterTitle(), getData("Risk Title"))) {
            error = "Failed to enter on 'Risk register title' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Risk title : '" + getData("Risk Title") + "'.");

        //Scope 
        if (!SeleniumDriverInstance.waitForElementByXpath(FR1_RiskRegister_PageObjects.scope())) {
            error = "Failed to wait for 'Scope' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(FR1_RiskRegister_PageObjects.scope(), getData("Scope"))) {
            error = "Failed to enter '" + getData("Scope") + "' into 'Scope' field.";
        }
      
        narrator.stepPassedWithScreenShot("Successfully entered.");

        return true;
    }

    public boolean impactType() {
        //Impact type
        if (!SeleniumDriverInstance.waitForElementByXpath(FR1_RiskRegister_PageObjects.impactTypeDropdown())) {
            error = "Failed to wait for 'Impact type' Dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR1_RiskRegister_PageObjects.impactTypeDropdown())) {
            error = "Failed to clcik 'Impact type' Dropdown.";
            return false;
        }
 
        //Check mark
        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(FR1_RiskRegister_PageObjects.selectAllImpactTypeTab())) {
         error = "Failed to click on 'Select all' impact type tab.";
         return false;
         }

        narrator.stepPassedWithScreenShot("Successfully selected 'Impact type'.");

        return true;
    }

//    public boolean processesToAssess() {
//        if (!SeleniumDriverInstance.waitForElementByXpath(FR1_RiskRegister_PageObjects.selectAllProcessesToBeAssessed())) {
//            error = "Failed to wait for 'Select all' processes to be assessed.";
//            return false;
//        }
//        pause(2000);
//        if (!SeleniumDriverInstance.clickElementbyXpath(FR1_RiskRegister_PageObjects.selectAllProcessesToBeAssessed())) {
//            error = "Failed to click on 'Select all' processes to be assessed.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully selected 'Processes to be assessed'.");
//
//        return true;
//    }

     public boolean processesToAssess() {
        if (!SeleniumDriverInstance.waitForElementByXpath(FR1_RiskRegister_PageObjects.selectProcessesToBeAssessed(getData("Processes to be assessed")))) {
            error = "Failed to wait for processes to be assessed values.";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(FR1_RiskRegister_PageObjects.selectProcessesToBeAssessed(getData("Processes to be assessed")))) {
            error = "Failed to select processes to be assessed.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected 'Processes to be assessed'.");

        return true;
    }
     
    public boolean riskAssessments() {
        //Type of Risk assessment
        if (!SeleniumDriverInstance.waitForElementByXpath(FR1_RiskRegister_PageObjects.riskAssessmentDropDown())) {
            error = "Failed to wait for 'Type of risk assessment' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR1_RiskRegister_PageObjects.riskAssessmentDropDown())) {
            error = "Failed to click on 'Type of risk assessment' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(FR1_RiskRegister_PageObjects.typeOfAssessment(getData("Type Of Risk Assessment")))) {
            error = "Failed to wait for '" + getData("Type Of Risk Assessment") + "'.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR1_RiskRegister_PageObjects.typeOfAssessment(getData("Type Of Risk Assessment")))) {
            error = "Failed to click on '" + getData("Type Of Risk Assessment") + "'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Type Of Risk Assessment : '" + getData("Type Of Risk Assessment") + "'.");

        //Is this a bowtie risk assesment?
        if (!SeleniumDriverInstance.waitForElementByXpath(FR1_RiskRegister_PageObjects.isThisBowtieDropDown())) {
            error = "Failed to wait for Is this a bowtie risk assesment? dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR1_RiskRegister_PageObjects.isThisBowtieDropDown())) {
            error = "Failed to click on Is this a bowtie risk assesment? dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(FR1_RiskRegister_PageObjects.typeOfAssessment(getData("Is this a bowtie risk assesment?")))) {
            error = "Failed to wait for '" + getData("Is this a bowtie risk assesment?") + "'.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR1_RiskRegister_PageObjects.typeOfAssessment(getData("Is this a bowtie risk assesment?")))) {
            error = "Failed to click on '" + getData("Is this a bowtie risk assesment?") + "'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Is this a bowtie risk assesment? : '" + getData("Is this a bowtie risk assesment?") + "'.");
        
        //Responsible person
        if (!SeleniumDriverInstance.waitForElementByXpath(FR1_RiskRegister_PageObjects.responsiblePersonDropDown())) {
            error = "Failed to wait for 'Responsible person' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR1_RiskRegister_PageObjects.responsiblePersonDropDown())) {
            error = "Failed to click on 'Responsible person' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(FR1_RiskRegister_PageObjects.responsiblePerson(getData("Responsible Person")))) {
            error = "Failed to wait for '" + getData("Responsible Person") + "'.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR1_RiskRegister_PageObjects.responsiblePerson(getData("Responsible Person")))) {
            error = "Failed to click on '" + getData("Responsible Person") + "'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Responsible Person : '" + getData("Responsible Person") + "'.");

        //Create risk assessment from
        if (!SeleniumDriverInstance.waitForElementByXpath(FR1_RiskRegister_PageObjects.createRiskAssessmentDropDown())) {
            error = "Failed to wait for 'Create risk assessment from' DropDown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR1_RiskRegister_PageObjects.createRiskAssessmentDropDown())) {
            error = "Failed to click on 'Create risk assessment from' DropDown.";
            return false;
        }

        String assessment = getData("Create risk assessment from");

        if (!SeleniumDriverInstance.waitForElementByXpath(FR1_RiskRegister_PageObjects.createRisckAssessment(getData("Create risk assessment from")))) {
            error = "Faileed to wait for '" + getData("Create risk assessment from") + "'.";
            return false;
        }
       
        if (!SeleniumDriverInstance.clickElementbyXpath(FR1_RiskRegister_PageObjects.createRisckAssessment(getData("Create risk assessment from")))) {
            error = "Failed to click on '" + getData("Create risk assessment from") + "'.";
            return false;
        }

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(FR1_RiskRegister_PageObjects.saveButton())) {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR1_RiskRegister_PageObjects.saveButton())) {
            error = "Failed to click on 'Save' button.";
            return false;
        }

         if (!SeleniumDriverInstance.WaitForLoaderToDisappear(FR1_RiskRegister_PageObjects.saveWait())) {
            error = "Failed to wait for 'Save wait' button.";
            return false;
        }

        //Validate save
        if (!SeleniumDriverInstance.waitForElementsByXpath(FR1_RiskRegister_PageObjects.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(FR1_RiskRegister_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");

        narrator.stepPassedWithScreenShot("Sucessfully saved the record and processflow moves to Edit phase");
        String processStatus2 = SeleniumDriverInstance.retrieveAttributeByXpath(FR1_RiskRegister_PageObjects.processFlowStatus("2. Edit / Under Review"),"class");
        if (!processStatus2.equalsIgnoreCase("step active")) {
        error = "Failed to move to 2. Edit / Under Review phase";
        return false;
        }
        
        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(FR1_RiskRegister_PageObjects.RiskRegisterRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());
       
        String retrieveStatusText = SeleniumDriverInstance.retrieveTextByXpath(FR1_RiskRegister_PageObjects.statusDropdown()); 
        if (!retrieveStatusText.equalsIgnoreCase("Open")) {
                error = "Failed to retrieve Reason for Status dropdown text ";
                return false;
            }


        return true;
    }

}
