/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.PageObjects.FR1_RiskRegister_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.PageObjects.FR2_FR10_RiskRegister_PageObjects;


/**
 *
 * @author Vijaya
 */
@KeywordAnnotation(
        Keyword = "FR3_ConductRiskAssessment_withoutBowtie_AlternateScenario2",
        createNewBrowserInstance = false
)
public class FR3_ConductRiskAssessment_withoutBowtie_AlternateScenario2 extends BaseClass {

    String error = "";

    public FR3_ConductRiskAssessment_withoutBowtie_AlternateScenario2() {

    }

    public TestResult executeTest() {

        if (!addRiskAssessment()) {
            return narrator.testFailed("Failed to add Risk Assessment record - " + error);
        }
        
        return narrator.finalizeTest("Successfully added Risk Assessment record");
    }

    public boolean addRiskAssessment() {
        //Risk Assessment Add button
         if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.riskAssessmentAddButton(), 5000)){
            error = "Failed to wait for Risk Assessment Add button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.riskAssessmentAddButton())){
            error = "Failed to click on Risk Assessment Add button";
            return false;
        }
        
        SeleniumDriverInstance.pause(8000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");
        
         //Processflow
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.riskAssessmentProcessflow())){
            error = "Failed to wait for 'Processflow' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.riskAssessmentProcessflow())){
            error = "Failed to click on 'Processflow' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Processflow in 1.Logging a Risk Assessment phase");
         String processStatusAdd = SeleniumDriverInstance.retrieveAttributeByXpath(FR1_RiskRegister_PageObjects.processFlowStatus("1. Logging a Risk Assessment"),"class");
        if (!processStatusAdd.equalsIgnoreCase("step active")) {
        error = "Failed to be in 1. Logging a Risk Assessment phase";
        return false;
        }
         
        
        //Risk Source dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.riskSourceDropdown())){
            error = "Failed to wait for Risk Source dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.riskSourceDropdown())){
            error = "Failed to click Risk Source dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.riskSourceDropdownExpand(testData.getData("Risk Source parent value"),"2"))) {
            error = "Failed to wait for Risk Source dropdown Expand";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.riskSourceDropdownExpand(testData.getData("Risk Source parent value"),"2"))) {
            error = "Failed to click Risk Source dropdown Expand";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Risk Source child value")))) {
            error = "Failed to wait for Risk Source child value";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Risk Source child value")))) {
            error = "Failed to click on Risk Source child value";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Risk Source checkbox not selected and so Risk source 1- 5 fields not poupulated");
        
        //Risk Source label
         if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.riskSourceLabelXpath())) {
            error = "Failed to wait for Risk Source label.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.riskSourceLabelXpath())) {
            error = "Failed to click on Risk Source label.";
            return false;
        }
         //Risk dropdown
         if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.riskLabel())){
            error = "Failed to click Risk label";
            return false;
        }
         
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.riskDropdown())){
            error = "Failed to wait for Risk dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.riskDropdown())){
            error = "Failed to click Risk dropdown";
            return false;
        }
        
        
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Risk")))) {
            error = "Failed to wait for Risk value";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Risk")))) {
            error = "Failed to click on Risk value";
            return false;
        }
         
        pause(1000);
       narrator.stepPassedWithScreenShot("Priority Unwanted event checkbox selected and Bowtie reference number fields are displayed");
       
        //Risk Checkbox
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.riskCheckbox())){
            error = "Failed to wait for Risk Checkbox";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.riskCheckbox())){
            error = "Failed to click Risk Checkbox";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Risk description, Layman's terms /Causes, Penalties / Consequences fields displayed");
        
        
         //Layman's terms /Causes
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.laymansTermsCauses())) {
            error = "Failed to wait for Layman's terms /Causes";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(FR2_FR10_RiskRegister_PageObjects.laymansTermsCauses(), getData("Layman's terms /Causes"))) {
            error = "Failed to enter text in Layman's terms /Causes";
            return false;
        } 
        
        //Penalties / Consequences
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.penaltiesConsequences())) {
            error = "Failed to wait for Penalties / Consequences";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(FR2_FR10_RiskRegister_PageObjects.penaltiesConsequences(), getData("Penalties / Consequences"))) {
            error = "Failed to enter text in Penalties / Consequences";
            return false;
        } 
        
        //Activity checklist
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.riskSourceDropdownExpand(testData.getData("Activity parent value"),"2"))) {
            error = "Failed to wait for Activity checklist Expand";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.riskSourceDropdownExpand(testData.getData("Activity parent value"),"2"))) {
            error = "Failed to click Activity checklist Expand";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.checklistSelect(getData("Activity child value")))) {
            error = "Failed to wait for Risk Source child value";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.checklistSelect(getData("Activity child value")))) {
            error = "Failed to click on Risk Source child value";
            return false;
        }
        
//        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.activitySelectAll())){
//            error = "Failed to wait for Activity checklist";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.activitySelectAll())){
//            error = "Failed to click on Activity checklist selectall";
//            return false;
//        }
        
         //Risk Owner
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.riskOwner())){
            error = "Failed to wait for Risk Owner";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.riskOwner())){
            error = "Failed to click Risk Owner";
            return false;
        }
        
        
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Risk Owner")))) {
            error = "Failed to wait for Risk Owner value";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Risk Owner")))) {
            error = "Failed to click on Risk Owner value";
            return false;
        }
        
        //Bowtie Reference Number
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.bowtieReferenceNumber())){
            error = "Failed to wait for Bowtie Reference Number";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.bowtieReferenceNumber())){
            error = "Failed to click Bowtie Reference Number";
            return false;
        }
        
        
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Bowtie Reference Number")))) {
            error = "Failed to wait for Bowtie Reference Number value";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Bowtie Reference Number")))) {
            error = "Failed to click on Bowtie Reference Number value";
            return false;
        }
        
        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.saveRiskAssessmentButton())) {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.saveRiskAssessmentButton())) {
            error = "Failed to click on 'Save' button.";
            return false;
        }

        if (!SeleniumDriverInstance.WaitForLoaderToDisappear(FR1_RiskRegister_PageObjects.saveWait())) {
            error = "Failed to wait for 'Save wait' button.";
            return false;
        }

        SeleniumDriverInstance.pause(15000);
        //Validate save
        if (!SeleniumDriverInstance.waitForElementsByXpath(FR1_RiskRegister_PageObjects.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(FR1_RiskRegister_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");

         narrator.stepPassedWithScreenShot("Successfully saved the record and processflow moved to 2. In Progress phase");
        String processStatus2 = SeleniumDriverInstance.retrieveAttributeByXpath(FR1_RiskRegister_PageObjects.processFlowStatus("2. In Progress"),"class");
        if (!processStatus2.equalsIgnoreCase("step active")) {
        error = "Failed to move to  2. In Progress phase";
        return false;
        }
        
        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(FR2_FR10_RiskRegister_PageObjects.riskAssessmentRecordNumber()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());
        
        String retrievePanel1Text = SeleniumDriverInstance.retrieveTextByXpath(FR2_FR10_RiskRegister_PageObjects.supportingInfoPanel()); 
        if (!retrievePanel1Text.equalsIgnoreCase("Supporting Information: Impact")) {
                error = "Failed to retrieve Supporting Information: Impact panel text ";
                return false;
            }
        
        String retrievePanel2Text = SeleniumDriverInstance.retrieveTextByXpath(FR2_FR10_RiskRegister_PageObjects.inherentRiskPanel()); 
        if (!retrievePanel2Text.equalsIgnoreCase("Inherent Risk")) {
                error = "Failed to retrieve Step 1: Inherent Risk panel text ";
                return false;
            }
        
        String retrievePanel3Text = SeleniumDriverInstance.retrieveTextByXpath(FR2_FR10_RiskRegister_PageObjects.controlsPanel()); 
        if (!retrievePanel3Text.equalsIgnoreCase("Controls")) {
                error = "Failed to retrieve Controls panel text ";
                return false;
            }
        
        //Expand Controls panel
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.controlsPanel())) {
            error = "Failed to wait for Controls panel";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.controlsPanel())) {
            error = "Failed to expand Controls panel";
            return false;
        }
        
        //Wait for controls records
        pause(55000);
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.controlsRecordsReplicate())) {
            error = "Failed to wait for Controls records to replicate";
            return false;
        }
        
        //close Controls panel
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.controlsPanel())) {
            error = "Failed to wait for Controls panel to close";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.controlsPanel())) {
            error = "Failed to close Controls panel";
            return false;
        }
        
        String retrievePanel5Text = SeleniumDriverInstance.retrieveTextByXpath(FR2_FR10_RiskRegister_PageObjects.residualRiskPanel()); 
        if (!retrievePanel5Text.equalsIgnoreCase("Residual Risk")) {
                error = "Failed to retrieve Step 3: Residual Risk panel text ";
                return false;
            }
                
        String retrievePanel4Text = SeleniumDriverInstance.retrieveTextByXpath(FR2_FR10_RiskRegister_PageObjects.actionsPanel()); 
        if (!retrievePanel4Text.equalsIgnoreCase("Actions")) {
                error = "Failed to retrieve Actions panel text ";
                return false;
            }
        
        //Additional information checkbox
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.additionalInformationCheckbox())) {
            error = "Failed to wait for Additional information checkbox.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.additionalInformationCheckbox())) {
            error = "Failed to click on Additional information checkbox";
            return false;
        }
        
        pause(2000);
        narrator.stepPassedWithScreenShot("Linked Strategic Objectives, Linked Compliance Requirements, Linked Key Risk Indicators, Related Incidents, Related Findings, Supporting Documents panels are displayed");
        
         String retrievePanel6Text = SeleniumDriverInstance.retrieveTextByXpath(FR2_FR10_RiskRegister_PageObjects.linkedStrategicObjectivesPanel()); 
        if (!retrievePanel6Text.equalsIgnoreCase("Linked Strategic Objectives")) {
                error = "Failed to retrieve Linked Strategic Objectives panel text ";
                return false;
            }
        
        String retrievePanel7Text = SeleniumDriverInstance.retrieveTextByXpath(FR2_FR10_RiskRegister_PageObjects.linkedComplianceRequirementsPanel()); 
        if (!retrievePanel7Text.equalsIgnoreCase("Linked Compliance Requirements")) {
                error = "Failed to retrieve Linked Compliance Requirements panel text ";
                return false;
            }
        
        String retrievePanel8Text = SeleniumDriverInstance.retrieveTextByXpath(FR2_FR10_RiskRegister_PageObjects.linkedKeyRiskIndicatorsPanel()); 
        if (!retrievePanel8Text.equalsIgnoreCase("Linked Key Risk Indicators")) {
                error = "Failed to retrieve Linked Key Risk Indicators panel text ";
                return false;
            }
        
        String retrievePanel9Text = SeleniumDriverInstance.retrieveTextByXpath(FR2_FR10_RiskRegister_PageObjects.relatedIncidentsPanel()); 
        if (!retrievePanel9Text.equalsIgnoreCase("Related Incidents")) {
                error = "Failed to retrieve Related Incidents panel text ";
                return false;
            }
        
        String retrievePanel10Text = SeleniumDriverInstance.retrieveTextByXpath(FR2_FR10_RiskRegister_PageObjects.relatedFindingsPanel()); 
        if (!retrievePanel10Text.equalsIgnoreCase("Related Findings")) {
                error = "Failed to retrieve Related Findings panel text ";
                return false;
            }
        
        String retrievePanel11Text = SeleniumDriverInstance.retrieveTextByXpath(FR2_FR10_RiskRegister_PageObjects.suppDocsPanel()); 
        if (!retrievePanel11Text.equalsIgnoreCase("Supporting Documents")) {
                error = "Failed to retrieve Supporting Documents panel text ";
                return false;
            }
        
        
        return true;
    }

}
