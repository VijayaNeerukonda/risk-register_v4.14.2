/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.PageObjects.FR1_RiskRegister_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.PageObjects.FR2_FR10_RiskRegister_PageObjects;
import org.openqa.selenium.JavascriptExecutor;


/**
 *
 * @author Vijaya
 */
@KeywordAnnotation(
        Keyword = "FR11_View_RelatedIncidents_MainScenario",
        createNewBrowserInstance = false
)
public class FR11_View_RelatedIncidents_MainScenario extends BaseClass {

    String error = "";

    public FR11_View_RelatedIncidents_MainScenario() {

    }

    public TestResult executeTest() {

        if (!viewRelatedIncidents()) {
            return narrator.testFailed("Failed to view Related Incidents - " + error);
        }
        
        return narrator.finalizeTest("Successfully viewed Related Incidents");
    }

    public boolean viewRelatedIncidents() {
        
        //Related Incidents panel
         if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.relatedIncidentsPanel(), 5000)){
            error = "Failed to wait for Related Incidents panel";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.relatedIncidentsPanel())){
            error = "Failed to click on Related Incidents panel";
            return false;
        }
        
        //Scroll down
        JavascriptExecutor js = (JavascriptExecutor)SeleniumDriverInstance.Driver;
        js.executeScript("window.scrollBy(0,1000)");


        SeleniumDriverInstance.pause(20000);
        narrator.stepPassedWithScreenShot("Incident Management view grid displayed");
        
        //Record to be opened
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.incidentManagementRecordOpen(), 5000)){
            error = "Failed to wait for Incident Management record to be opened";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.incidentManagementRecordOpen())){
            error = "Failed to click on Incident Management record to be opened";
            return false;
        }
        
        pause(60000);
        
        //Incident Management Processflow
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.incidentManagementProcessflow())){
            error = "Failed to wait for 'Processflow' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.incidentManagementProcessflow())){
            error = "Failed to click on 'Processflow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Incident Management record opens in 3.Under Investigation or 4. Awaiting Sign off Phase");
        
        
        return true;
    }

}
