/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.PageObjects.FR1_RiskRegister_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.PageObjects.FR2_FR10_RiskRegister_PageObjects;


/**
 *
 * @author Vijaya
 */
@KeywordAnnotation(
        Keyword = "FR2_ViewandEdit_ReasonforReviewHistory_AlternateScenario",
        createNewBrowserInstance = false
)
public class FR2_ViewandEdit_ReasonforReviewHistory_AlternateScenario extends BaseClass {

    String error = "";

    public FR2_ViewandEdit_ReasonforReviewHistory_AlternateScenario() {

    }

    public TestResult executeTest() {

        if (!reasonForRequestNotApprove()) {
            return narrator.testFailed("Failed not Approving Register a reason for request - " + error);
        }
       
        return narrator.finalizeTest("Successfully did not Approved Register a reason for request");
    }

    public boolean reasonForRequestNotApprove() {
        
         //Status dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.statusDropdownXpath())) {
            error = "Failed to wait for Status dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.statusDropdownXpath())) {
            error = "Failed to click on Status dropdown.";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Status")))) {
            error = "Failed to wait for '" + getData("Status") + "' list.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Status")))) {
            error = "Failed to click on '" + getData("Status") + "' list.";
            return false;
        }
        
        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.saveButton())) {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.saveButton())) {
            error = "Failed to click on 'Save' button.";
            return false;
        }

        if (!SeleniumDriverInstance.WaitForLoaderToDisappear(FR1_RiskRegister_PageObjects.saveWait())) {
            error = "Failed to wait for 'Save wait' button.";
            return false;
        }

        //Validate save
        if (!SeleniumDriverInstance.waitForElementsByXpath(FR1_RiskRegister_PageObjects.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(FR1_RiskRegister_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");

        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(FR1_RiskRegister_PageObjects.RiskRegisterRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());
        
        
        //Review request History Tab
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.reviewRequestHistoryTab())){
            error = "Failed to wait for Review request History Tab";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.reviewRequestHistoryTab())){
            error = "Failed to click on Review request History Tab";
            return false;
        }
        
        //Search Filter button
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.searchFilterButton())){
            error = "Failed to wait for Search Filter button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.searchFilterButton())){
            error = "Failed to click on Search Filter button";
            return false;
        }
        
        pause(3000);
         //Search  button
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.searchButton())){
            error = "Failed to wait for Search  button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.searchButton())){
            error = "Failed to click on Search  button";
            return false;
        }
        
        pause(15000);
        //Record to be Approved
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.recordTobeApproved())){
            error = "Failed to wait for Record to be Approved";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.recordTobeApproved())){
            error = "Failed to click on Record to be Approved";
            return false;
        }
        
        
        //Processflow
        pause(5000);
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.processflowXpath())){
            error = "Failed to wait for Processflow";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.processflowXpath())){
            error = "Failed to click on Processflow";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Processflow in Edit Phase");
        
       
        //Reason for Review History Save button
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.historySaveButton())){
            error = "Failed to wait for Reason for Review History Save button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.historySaveButton())){
            error = "Failed to click on Reason for Review History Save button";
            return false;
        }
        
        pause(15000);
        String[] retrieveHistoryRecordNumber = SeleniumDriverInstance.retrieveTextByXpath(FR2_FR10_RiskRegister_PageObjects.historyRecordNumber_xpath()).split("#");
        setRecordId(retrieveHistoryRecordNumber[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());
         narrator.stepPassedWithScreenShot("Didnot update Date approved and Approved by fields and Processflow remains in Edit phase");

        return true;
    }

}
