/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.PageObjects.FR1_RiskRegister_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.PageObjects.FR2_FR10_RiskRegister_PageObjects;


/**
 *
 * @author Vijaya
 */
@KeywordAnnotation(
        Keyword = "FR8_Capture_InherentRisk_MainScenario",
        createNewBrowserInstance = false
)
public class FR8_Capture_InherentRisk_MainScenario extends BaseClass {

    String error = "";

    public FR8_Capture_InherentRisk_MainScenario() {

    }

    public TestResult executeTest() {

        if (!addInherentRisk()) {
            return narrator.testFailed("Failed to add Inherent Risk - " + error);
        }
        
        return narrator.finalizeTest("Successfully added Inherent Risk record");
    }

    public boolean addInherentRisk() {
        
        //Step 1: Inherent Risk panel
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.inherentRiskPanel(), 5000)){
            error = "Failed to wait for Inherent Risk panel";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.inherentRiskPanel())){
            error = "Failed to click on Inherent Risk panel";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Impact - Inherent Risk editable grid is displayed");
         
        //Impact - Inherent Risk Add button
         if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.impactInherentRiskAddButton(), 5000)){
            error = "Failed to wait for Impact - Inherent Risk Add button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.impactInherentRiskAddButton())){
            error = "Failed to click on Impact - Inherent Risk Add button";
            return false;
        }
        
        
        narrator.stepPassedWithScreenShot("New row displayed");
        
        //Consequence type dropdown
        pause(2000);
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.consequenceTypeIIRDropdown())){
            error = "Failed to wait for Impact - Inherent Risk - Consequence type dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.consequenceTypeIIRDropdown())){
            error = "Failed to click Impact - Inherent Risk - Consequence type dropdown";
            return false;
        }
        
        pause(2000);
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.singleSelectIndex(getData("Impact - Inherent Risk Consequence type dropdown value"),"1"))) {
            error = "Failed to wait for Impact - Inherent Risk Consequence type dropdown value";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.singleSelectIndex(getData("Impact - Inherent Risk Consequence type dropdown value"),"1"))) {
            error = "Failed to click on Impact - Inherent Risk Consequence type dropdown value";
            return false;
        }
        
        //Consequence dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.consequenceIIRDropdown())){
            error = "Failed to wait for Impact - Inherent Risk - Consequence dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.consequenceIIRDropdown())){
            error = "Failed to click Impact - Inherent Risk - Consequence dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.consequenceDropdownExpand(testData.getData("Consequence parent value"),"1"))) {
            error = "Failed to wait for Impact - Inherent Risk - Consequence dropdown Expand";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.consequenceDropdownExpand(testData.getData("Consequence parent value"),"1"))) {
            error = "Failed to click Impact - Inherent Risk - Consequence dropdown Expand";
            return false;
        }
            
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Impact - Inherent Risk Consequence dropdown value")))) {
            error = "Failed to wait for Impact - Inherent Risk - Consequence dropdown value";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Impact - Inherent Risk Consequence dropdown value")))) {
            error = "Failed to click on Impact - Inherent Risk - Consequence dropdown value";
            return false;
        }
        
        //Likelihood rating
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.likelihoodRatingInherentRisk())){
            error = "Failed to wait for Likelihood rating dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.likelihoodRatingInherentRisk())){
            error = "Failed to click Likelihood rating dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Likelihood rating")))) {
            error = "Failed to wait for Likelihood rating dropdown value";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Likelihood rating")))) {
            error = "Failed to click on Likelihood rating dropdown value";
            return false;
        }
        
         
        narrator.stepPassedWithScreenShot("Risk rating and Risk rating description fields populated with values");
        
        
        //Impact - Inherent Risk Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.impactInherentRiskSaveButton())) {
            error = "Failed to wait for Impact - Inherent Risk 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.impactInherentRiskSaveButton())) {
            error = "Failed to click on Impact - Inherent Risk 'Save' button.";
            return false;
        }

//        if (!SeleniumDriverInstance.WaitForLoaderToDisappear(FR1_RiskRegister_PageObjects.saveWait())) {
//            error = "Failed to wait for 'Save wait' button.";
//            return false;
//        }
//
//         
//        //Validate save
//        if (!SeleniumDriverInstance.waitForElementsByXpath(FR1_RiskRegister_PageObjects.validateSave())) {
//            error = "Failed to wait for Save validation.";
//            return false;
//        }
//        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(FR1_RiskRegister_PageObjects.validateSave());
//
//        if (!SaveFloat.equals("Record saved")) {
//            narrator.stepPassedWithScreenShot("Failed to save record.");
//            return false;
//        }
//        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");

         pause(30000);
        narrator.stepPassedWithScreenShot("Successfully saved Impact Inherent Risk record");
                
        
        //Residual Risk panel
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.residualRiskPanel())) {
            error = "Failed to wait for Residual Risk panel";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.residualRiskPanel())) {
            error = "Failed to click on Residual Risk panel";
            return false;
        }
        
        
        //Residual Risk search button
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.residualRiskSearchButton())) {
            error = "Failed to wait for Residual Risk search button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.residualRiskSearchButton())) {
            error = "Failed to click on Residual Risk search button";
            return false;
        }
        
        //Residual Risk search button2
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.residualRiskSearchButton2())) {
            error = "Failed to wait for Residual Risk search button2";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.residualRiskSearchButton2())) {
            error = "Failed to click on Residual Risk search button2";
            return false;
        }
        
//        String retrieveGridText = SeleniumDriverInstance.retrieveTextByXpath(FR2_FR10_RiskRegister_PageObjects.impactResidualRiskGrid()); 
//        if (!retrieveGridText.equalsIgnoreCase("Impact - Residual Risk")) {
//                error = "Failed to retrieve Impact - Residual Risk Grid text ";
//                return false;
//            }
        
        pause(15000);
         //Wait for Imapct Risk records to replicate
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.impactRiskRecordsReplicate())) {
            error = "Failed to wait for Imapct Risk records to replicate";
            return false;
        }
        
        return true;
    }

}
