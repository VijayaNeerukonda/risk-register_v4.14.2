/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.PageObjects.FR1_RiskRegister_PageObjects;


/**
 *
 * @author Vijaya
 */
@KeywordAnnotation(
        Keyword = "UC_RSM_0102_Capture_RiskRegisterfromExisting_OptionalScenario",
        createNewBrowserInstance = false
)
public class UC_RSM_0102_Capture_RiskRegisterfromExisting_OptionalScenario extends BaseClass {

    String error = "";

    public UC_RSM_0102_Capture_RiskRegisterfromExisting_OptionalScenario() {

    }

    public TestResult executeTest() {

        if (!uploadSupportingDocuments()) {
            return narrator.testFailed("Failed to upload Supporting Documents - " + error);
        }
       
        return narrator.finalizeTest("Successfully uploaded Supporting Documents");
    }

    public boolean uploadSupportingDocuments() {
        //Supporting documents tab
       if (!SeleniumDriverInstance.waitForElementByXpath(FR1_RiskRegister_PageObjects.supportingDocumentsTab()))
       {
        error = "Failed to locate Supporting Documents tab";
        return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(FR1_RiskRegister_PageObjects.supportingDocumentsTab()))
        {
        error = "Failed to click on Supporting Documents tab";
        return false;
        }

        //Click upload document
        if(!SeleniumDriverInstance.waitForElementByXpath(FR1_RiskRegister_PageObjects.uploadDocument())){
        error = "Failed to wait for Supporting documents 'Upload document' link.";
        return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(FR1_RiskRegister_PageObjects.uploadDocument())){
        error = "Failed to click on Supporting documents 'Upload document' link.";
        return false;
        }

        if(!SeleniumDriverInstance.uploadAFile("C:\\Users\\Vijaya\\Documents\\Vijaya Docs\\sample document.xlsx")){
        error = "Failed to upload Supporting documents";
        return false;
        }

        //Upload hyperlink to Supporting documents
        //click link
        if(!SeleniumDriverInstance.waitForElementByXpath(FR1_RiskRegister_PageObjects.linkToADocument())){
        error = "Failed to wait for Supporting documents 'Link box' link.";
        return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(FR1_RiskRegister_PageObjects.linkToADocument())){
        error = "Failed to click on Supporting documents 'Link box' link.";
        return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully clicked Supporting documents 'Upload Hyperlink box'.");

        //switch to new window
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
        error = "Failed to switch to new window or tab.";
        return false;
        }

        //URL https
        if(!SeleniumDriverInstance.waitForElementByXpath(FR1_RiskRegister_PageObjects.LinkURL())){
        error = "Failed to wait for 'URL value' field.";
        return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(FR1_RiskRegister_PageObjects.LinkURL(), getData("Document Link") )){
        error = "Failed to enter '" + getData("Document Link") + "' into 'URL value' field.";
        return false;
        }
        narrator.stepPassedWithScreenShot("Document link : '" + getData("Document Link") + "'.");

        //Title
        if(!SeleniumDriverInstance.waitForElementByXpath(FR1_RiskRegister_PageObjects.urlTitle())){
        error = "Failed to wait for 'Url Title' field.";
        return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(FR1_RiskRegister_PageObjects.urlTitle(), getData("URL Title"))){
        error = "Failed to enter '" + getData("URL Title") + "' into 'Url Title' field.";
        return false;
        }
        narrator.stepPassedWithScreenShot("URL Title : '" + getData("URL Title") + "'.");

        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(FR1_RiskRegister_PageObjects.urlAddButton())){
        error = "Failed to wait for 'Add' button.";
        return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR1_RiskRegister_PageObjects.urlAddButton())){
        error = "Failed to click on 'Add' button.";
        return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the 'Add' button.");
        narrator.stepPassed("Successfully uploaded '" + getData("Title") + "' document using '" + getData("Document Link") + "' Link.");

        //Switch to iframe
        if (!SeleniumDriverInstance.swithToFrameByName(FR1_RiskRegister_PageObjects.iframeName()))
        {
        error = "Failed to switch to frame ";

        }
        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");


        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(FR1_RiskRegister_PageObjects.saveButton())) {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR1_RiskRegister_PageObjects.saveButton())) {
            error = "Failed to click on 'Save' button.";
            return false;
        }

        if (!SeleniumDriverInstance.WaitForLoaderToDisappear(FR1_RiskRegister_PageObjects.saveWait())) {
            error = "Failed to wait for 'Save wait' button.";
            return false;
        }

        //Validate save
        if (!SeleniumDriverInstance.waitForElementsByXpath(FR1_RiskRegister_PageObjects.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(FR1_RiskRegister_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");

        
        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(FR1_RiskRegister_PageObjects.RiskRegisterRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());
        

        return true;
    }

}
