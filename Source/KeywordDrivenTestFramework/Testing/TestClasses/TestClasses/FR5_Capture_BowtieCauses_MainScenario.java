/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.PageObjects.FR1_RiskRegister_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.PageObjects.FR2_FR10_RiskRegister_PageObjects;


/**
 *
 * @author Vijaya
 */
@KeywordAnnotation(
        Keyword = "FR5_Capture_BowtieCauses_MainScenario",
        createNewBrowserInstance = false
)
public class FR5_Capture_BowtieCauses_MainScenario extends BaseClass {

    String error = "";

    public FR5_Capture_BowtieCauses_MainScenario() {

    }

    public TestResult executeTest() {

        if (!addCauses()) {
            return narrator.testFailed("Failed to add Causes - " + error);
        }
        
        return narrator.finalizeTest("Successfully added Causes");
    }

    public boolean addCauses() {
        //Causes panel
         if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.causesPanel(), 5000)){
            error = "Failed to wait for Causes panel";
            return false;
        }
         
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.causesPanel())){
            error = "Failed to click on Causes panel";
            return false;
        }
        
        //Causes Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.causesAddButton())){
            error = "Failed to wait for Causes Add button";
            return false;
        }
         
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.causesAddButton())){
            error = "Failed to click on Causes Add button";
            return false;
        }
        
        SeleniumDriverInstance.pause(20000);
         //Processflow
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.causesProcessflow())){
            error = "Failed to wait for Causes Processflow";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.causesProcessflow())){
            error = "Failed to click on Causes Processflow";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Processflow in Add phase");
        String processStatusAdd = SeleniumDriverInstance.retrieveAttributeByXpath(FR1_RiskRegister_PageObjects.processFlowStatus("Add phase"),"class");
        if (!processStatusAdd.equalsIgnoreCase("step active")) {
        error = "Failed to be in Add phase";
        return false;
        }
        
        //Cause title
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.causeTitle())) {
            error = "Failed to wait for Cause title";
            return false;
        }
        
        if (!SeleniumDriverInstance.enterTextByXpath(FR2_FR10_RiskRegister_PageObjects.causeTitle(), getData("Cause title"))) {
            error = "Failed to enter text in Cause title";
            return false;
        } 
        
        //Cause 
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.causeField())) {
            error = "Failed to wait for Cause";
            return false;
        }
        
        if (!SeleniumDriverInstance.enterTextByXpath(FR2_FR10_RiskRegister_PageObjects.causeField(), getData("Cause"))) {
            error = "Failed to enter text in Cause";
            return false;
        }
        
               
        //Controls dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.controlsDropdown())){
            error = "Failed to wait for Controls dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.controlsDropdown())){
            error = "Failed to click Controls dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.controlsDropdownExpand(testData.getData("Controls parent value"),"1"))) {
            error = "Failed to wait for Controls dropdown Expand";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.controlsDropdownExpand(testData.getData("Controls parent value"),"1"))) {
            error = "Failed to click Controls dropdown Expand";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.checklistSelect(getData("Controls child value")))) {
            error = "Failed to wait for Controls child value";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.checklistSelect(getData("Controls child value")))) {
            error = "Failed to click on Controls child value";
            return false;
        }
        
        //Controls Save button
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.controlsSaveButton())){
            error = "Failed to wait for Controls Save button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.controlsSaveButton())){
            error = "Failed to click Controls Save button";
            return false;
        }
        
        if (!SeleniumDriverInstance.WaitForLoaderToDisappear(FR1_RiskRegister_PageObjects.saveWait())) {
            error = "Failed to wait for 'Save wait' button.";
            return false;
        }

       
        //Validate save
        if (!SeleniumDriverInstance.waitForElementsByXpath(FR1_RiskRegister_PageObjects.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(FR1_RiskRegister_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");

        narrator.stepPassedWithScreenShot("Successfully saved the record and processflow moved to Editphase");
        String processStatusEdit = SeleniumDriverInstance.retrieveAttributeByXpath(FR1_RiskRegister_PageObjects.processFlowStatus("Edit phase"),"class");
        if (!processStatusEdit.equalsIgnoreCase("step active")) {
        error = "Failed to be in Edit phase";
        return false;
        }
        
        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(FR2_FR10_RiskRegister_PageObjects.causesRecordNumber()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());
        
        pause(5000);
        //Replication to Preventive controls
         if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.preventiveControlsReplication())){
            error = "Failed to wait for Preventive controls replicated records";
            return false;
        }
         
        return true;
    }

}
