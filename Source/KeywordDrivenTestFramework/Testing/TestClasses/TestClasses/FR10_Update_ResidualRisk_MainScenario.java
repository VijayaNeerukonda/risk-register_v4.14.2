/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.PageObjects.FR1_RiskRegister_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.PageObjects.FR2_FR10_RiskRegister_PageObjects;
import org.openqa.selenium.JavascriptExecutor;


/**
 *
 * @author Vijaya
 */
@KeywordAnnotation(
        Keyword = "FR10_Update_ResidualRisk_MainScenario",
        createNewBrowserInstance = false
)
public class FR10_Update_ResidualRisk_MainScenario extends BaseClass {

    String error = "";

    public FR10_Update_ResidualRisk_MainScenario() {

    }

    public TestResult executeTest() {

        if (!updateResidualRisk()) {
            return narrator.testFailed("Failed to update residual risk - " + error);
        }
        
        return narrator.finalizeTest("Successfully updated residual risk");
    }

    public boolean updateResidualRisk() {
        
//        //Residual Risk records to open
//        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.impactRiskRecordsReplicate())) {
//            error = "Failed to wait for Residual Risk records";
//            return false;
//        }
//        
//        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.impactRiskRecordsReplicate())) {
//            error = "Failed to click on Residual Risk records";
//            return false;
//        }
                
                
        //Likelihood Rating dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.residualRiskLikelihoodRatingDropdown())){
            error = "Failed to wait for Likelihood Rating dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.residualRiskLikelihoodRatingDropdown())){
            error = "Failed to click Likelihood Rating dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Likelihood Rating")))) {
            error = "Failed to wait for Likelihood Rating dropdown value";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.singleSelect(getData("Likelihood Rating")))) {
            error = "Failed to click on Likelihood Rating dropdown value";
            return false;
        }
         
        narrator.stepPassedWithScreenShot("Risk Rating and Risk Rating Description fields are populated");
        
        //Residual Risk Save button
         pause(1000);
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.residualRiskSaveButton())) {
            error = "Failed to wait for Residual Risk Save button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.residualRiskSaveButton())) {
            error = "Failed to click on Residual Risk Save button";
            return false;
        }

       if (!SeleniumDriverInstance.WaitForLoaderToDisappear(FR1_RiskRegister_PageObjects.saveWait())) {
            error = "Failed to wait for 'Save wait' button.";
            return false;
        }

        pause(40000);
//        //Validate save
//        if (!SeleniumDriverInstance.waitForElementsByXpath(FR1_RiskRegister_PageObjects.validateSave())) {
//            error = "Failed to wait for Save validation.";
//            return false;
//        }
//        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(FR1_RiskRegister_PageObjects.validateSave());
//
//        if (!SaveFloat.equals("Record saved")) {
//            narrator.stepPassedWithScreenShot("Failed to save record.");
//            return false;
//        }
//        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");
//
//        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(FR2_FR10_RiskRegister_PageObjects.residualRiskRecordNumber()).split("#");
//        setRecordId(retrieveMessage[1]);
//        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());
//        
        
        //Residual Rating on Risk Assessment
         if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.residualRatingLabel())) {
            error = "Failed to wait for Residual Rating on Risk Assessment";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.residualRatingLabel())) {
            error = "Failed to click on Residual Rating on Risk Assessment";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Residual Rating updated");
       
        return true;
    }

}
