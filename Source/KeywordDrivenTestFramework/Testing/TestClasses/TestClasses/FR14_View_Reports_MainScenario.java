/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.PageObjects.FR1_RiskRegister_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.PageObjects.FR2_FR10_RiskRegister_PageObjects;
import org.openqa.selenium.JavascriptExecutor;


/**
 *
 * @author Vijaya
 */
@KeywordAnnotation(
        Keyword = "FR14_View_Reports_MainScenario",
        createNewBrowserInstance = false
)
public class FR14_View_Reports_MainScenario extends BaseClass {

    String error = "";

    public FR14_View_Reports_MainScenario() {

    }

    public TestResult executeTest() {

        if (!viewReports()) {
            return narrator.testFailed("Failed to view reports - " + error);
        }
        
        return narrator.finalizeTest("Successfully viewed reports");
    }

    public boolean viewReports() {
        
         //Navigate to Intergrated Risk Register
        if(!SeleniumDriverInstance.waitForElementByXpath(FR1_RiskRegister_PageObjects.navigate_IntegratedRiskRegister())){
            error = "Failed to wait for 'Integrated Risk Register'";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR1_RiskRegister_PageObjects.navigate_IntegratedRiskRegister())){
            error = "Failed to click on 'Integrated Risk Register'";
            return false;
        }
        
        //Set the page as a parent page
        String parentWindow = SeleniumDriverInstance.Driver.getWindowHandle();


        //Search Button
        pause(5000);
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.searchButtonOnSearchpage()))
        {
        error = "Failed to locate Search Button";
        return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.searchButtonOnSearchpage()))
        {
        error = "Failed to click Search Button";
        return false;
        }

        pause(3000);
        narrator.stepPassedWithScreenShot("Successfully showing the list of all Risk Register records");

        //Reports Button on search page

        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.reportsButton()))
        {
        error = "Failed to locate Reports Button on search page";
        return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.reportsButton()))
        {
        error = "Failed to click Reports Button on search page";
        return false;
        }

        //Click view report
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.viewReport()))
        {
        error = "Failed to locate view report on right side";
        return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.viewReport()))
        {
        error = "Failed to click view report on right side";
        return false;
        }

        //switch to new window
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
        error = "Failed to switch to new window or tab.";
        return false;
        }

        //Click continue
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.continueButton()))
        {
        error = "Failed to locate continue button on popup";
        return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.continueButton()))
        {
        error = "Failed to click continue button on popup";
        return false;
        }


        //switch to new window
        if(!SeleniumDriverInstance.switchToWindow()){
        error = "Failed to switch to new window or tab.";
        return false;
        }


        pause(15000);
        //Scroll down
        JavascriptExecutor js = (JavascriptExecutor)SeleniumDriverInstance.Driver;
        js.executeScript("window.scrollBy(0,1000)");

        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully showing all records in the View Report");

        //Scroll up
        js.executeScript("window.scrollBy(0,-1000)");

        pause(3000);
        //Export dropdown menu
//        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.exportDropdownMenu()))
//        {
//        error = "Failed to locate Export dropdown menu";
//        return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.exportDropdownMenu()))
//        {
//        error = "Failed to click Export dropdown menu";
//        return false;
//        }
//
//        //Export Word
//        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.exportWord()))
//        {
//        error = "Failed to locate Export Word";
//        return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.exportWord()))
//        {
//        error = "Failed to click Export Word";
//        return false;
//        }

        //Close the current window
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);
        System.out.println(SeleniumDriverInstance.Driver.getTitle());

        //switch to parent window iframe
        SeleniumDriverInstance.switchToDefaultContent();
        SeleniumDriverInstance.swithToFrameByName("ifrMain");

        pause(8000);
        //Click Full report
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.fullReport()))
        {
        error = "Failed to locate Full report on right side";
        return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.fullReport()))
        {
        error = "Failed to click Full report on right side";
        return false;
        }

        //switch to new window
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
        error = "Failed to switch to new window or tab.";
        return false;
        }

        //Click continue
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.continueButton()))
        {
        error = "Failed to locate continue button on popup";
        return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.continueButton()))
        {
        error = "Failed to click continue button on popup";
        return false;
        }


        //switch to new window
        if(!SeleniumDriverInstance.switchToWindow()){
        error = "Failed to switch to new window or tab.";
        return false;
        }

        pause(15000);
        //Scroll down
        js.executeScript("window.scrollBy(0,1000)");

        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully showing all records in the View Report");

        //Scroll up
        js.executeScript("window.scrollBy(0,-1000)");

        pause(3000);
        //Export dropdown menu
//        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.exportDropdownMenu()))
//        {
//        error = "Failed to locate Export dropdown menu";
//        return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.exportDropdownMenu()))
//        {
//        error = "Failed to click Export dropdown menu";
//        return false;
//        }
//
//        //Export Word
//        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_FR10_RiskRegister_PageObjects.exportWord()))
//        {
//        error = "Failed to locate Export Word";
//        return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_FR10_RiskRegister_PageObjects.exportWord()))
//        {
//        error = "Failed to click Export Word";
//        return false;
//        }

        //Close the current window
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);
        System.out.println(SeleniumDriverInstance.Driver.getTitle());

        //switch to parent window iframe
        SeleniumDriverInstance.switchToDefaultContent();
        SeleniumDriverInstance.swithToFrameByName("ifrMain");
        return true;
    }

}
