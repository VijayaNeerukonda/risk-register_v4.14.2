/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.PageObjects;

import KeywordDrivenTestFramework.Core.BaseClass;

/**
 *
 * @author syotsi
 */
public class FR1_RiskRegister_PageObjects extends BaseClass {

    public static String navigate_IntegratedRiskRegister() {
        return "(//label[contains(text(),'Risk Register')])[2]";
    }
    
    public static String RegisterRisk_button_Add() {
        return "//div[@id='btnActAddNew']";
    }
    
    public static String riskRegisterScopeTab() {
        return "//li[@id='tab_0029D15E-8A6A-403E-8C5F-67B994DA330B']";
    }
    
    public static String RegisterRisk_button_Processflow() {
        return "//div[@id='btnProcessFlow_form_3AB45051-222E-416C-AAD3-86B2EDA98BED']";
    }
    
    public static String processFlowStatus(String phase)
    {
    return "(//div[text()='"+phase+"'])[2]/parent::div";
    }
    
    public static String processFlowStatusChild(String phase)
    {
    return "(//div[text()='"+phase+"'])[3]/parent::div";
    }
    
    public static String reasonForRequestXpath() {
        return "//div[@id='control_460A1A95-6C2F-4764-9631-1DDB8912196B']//li";
    }
    
    public static String reasonForRequestLabelXpath() {
        return "//div[@id='control_5311A313-47B0-4EA3-BB25-5471E1918BF4']";
    }
    
    public static String linkedIncidentLabelXpath() {
        return "//div[@id='control_B7D3A766-70E7-4918-AB11-05879E888819']";
    }
    
    
    public static String riskAssessmentGrid() {
        return "//div[@id='control_3BDB074E-C37E-48F3-85C5-CDBE25069F9E']//div[text()='Risk Assessment']";
    }
    
            
    public static String statusDropdown() {
        return "//div[@id='control_87EEE5E5-7E0D-4E0B-A183-DD2E5613D0ED']//li";
    }
    
    public static String riskAssessmentRecordToBeOpened() {
        return "//div[@id='control_3BDB074E-C37E-48F3-85C5-CDBE25069F9E']//div[contains(text(),'Drag a column header and drop it here to group by that column')]/../div[3]//tr[1]";
    }
    
    public static String riskAssessmentProcessflow() {
        return "//div[@id='btnProcessFlow_form_3A80F1F4-AE90-47B2-B9BB-0C920589C04D']";
    }
    
    public static String anyReasonForRequest(String text) {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + text + "']";
    }
    
    public static String projectLink() {
        return "//div[@id='control_2E6AB32F-5F8A-49BC-88E1-A8CD606E9B11']/div[1]";
    }
    
    public static String projectDropdown() {
        return "//div[@id='control_8211E2EF-0E29-4A28-8689-206A31258F13']//li";
    }
    
    public static String anyProject(String text) {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + text + "']";
    }
    
    public static String relatedStakeholder() {
        return "//div[@id='control_3553F3D3-D76D-4205-A506-7450A6163C03']//li";
    }
    
    public static String businessUnitDropdown() {
        return "//div[@id='control_A1B23A4E-893B-46AB-9C47-AF967BE14AB7']//li";
    }
    
    public static String businessUnitexpand(String text){
        return "//a[text()='"+text+"']/../i";
    }
    
    public static String businessUnitValue(String text){
        return "//a[text()='"+text+"']";
    }
    
    public static String riskRegisterTitle() {
        return "//div[@id='control_A280BA0F-593B-4A15-A93D-4B2522C64ED7']/div[1]/div[1]/input";
    }

    public static String scope() {
        return "//div[@id='control_ED3FCD7C-F73F-4CE8-9030-08CE80DDA7CD']//textarea";
    }
    
    public static String impactTypeDropdown() {
        return "//div[@id='control_D0E8F3FA-BD27-4584-B51E-6B49F16D0480']//li";
    }
    
    public static String selectImpactDropDown(){
        return "//div[@id='control_D0E8F3FA-BD27-4584-B51E-6B49F16D0480']/div[1]//b[@class='select3-down drop_click']";
    }
    
    public static String selectAllImpactTypeTab() {
        return "//div[@id='control_D0E8F3FA-BD27-4584-B51E-6B49F16D0480']//b[@class='select3-all']";
    }
    
     public static String processesToBeAssessed() {
        return "//div[@id='control_0AB10D0E-208C-4EBA-80F7-A2E648A3D808']//a[@class='select3-choice']";
    }

     public static String selectProcessesToBeAssessed(String text)
    {
       return "(//a[text()='" + text + "']/i[1])[1]";
    }
     
    public static String selectAllProcessesToBeAssessed() {
        return "//div[@id='control_0AB10D0E-208C-4EBA-80F7-A2E648A3D808']//b[@class='select3-all']";
    }

     public static String riskAssessmentDropDown() {
        return "//div[@id='control_7980E64B-9FFC-4251-8A14-227AEEFE7309']//li";
    }

    public static String typeOfAssessment(String text) {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + text + "']";
    }

    public static String isThisBowtieDropDown() {
        return "//div[@id='control_8CC137FF-3CE3-4C38-8C0B-0D0B0D51993F']//li";
    }

    
            
    public static String responsiblePersonDropDown() {
        return "//div[@id='control_25EB2D38-F2AA-4689-94E5-36BABF04BBF9']//li";
    }

    public static String responsiblePerson(String text) {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + text + "']";
    }
    
    public static String createRiskAssessmentDropDown() {
        return "//div[@id='control_D4AC91B6-E1EF-447E-AE02-CFBCF78907A1']//li";
    }

    public static String createRisckAssessment(String text) {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + text + "']";
    }
    
    public static String saveButton(){
        return "//div[@id='btnSave_form_3AB45051-222E-416C-AAD3-86B2EDA98BED']";
    }
    public static String saveWait(){
        return ".//div[text()='Saving...']";
    }
    
    public static String validateSave() {
        return "//div[@class='ui floating icon message transition visible']";
    }

     
    public static String RiskRegisterRecordNumber_xpath() {
        return "//div[@id='form_3AB45051-222E-416C-AAD3-86B2EDA98BED']//div[contains(text(),'- Record #')]";
    }
 
    //email verification
    public static String office_URL() {
        return "https://www.office.com";
    }
    
    public static String office_signin() {
        return ".//a[contains(text(),'Sign in')]";
    }
    
    public static String office_email_id(){
        return ".//input[@type='email']";
    }
    
    public static String email_next_btn(){
        return ".//input[@value='Next']";
    }
    
    public static String office_password(){
        return "//input[@type='password']";
    }
    
    
    public static String office_signin_btn()
    {
        return "//input[@value='Sign in']";
    }
    
     public static String office_No_btn()
    {
        return "//input[@value='No']";
    }
    
    public static String outlook_icon(){
        return ".//div[@id='ShellMail_link_text']";
    }
    
    public static String other_folder(){
        return "//span[text()='Other']";
    }
    
    
    public static String email_notification(String recordNumber){
        return ".//span[text()='NON PRODUCTION - IsoMetrix Risk Register #"+recordNumber+" has been logged.']";
    }
    
   
    public static String linkBackToRecord_Link()
    {
        return ".//a[@data-auth='NotApplicable']";
    }
    
     public static String Username() {
        return "//input[@id='txtUsername']";
    }

    public static String Password() {
        return "//input[@id='txtPassword']";
    }

    public static String LoginBtn() {
        return "//div[@id='btnLoginSubmit']";
    }
    
    public static String VerificationXpath(String recordNumber) {
        return "(//div[contains(@class,'transition visible')]//div[text()='- Record #"+recordNumber+"'])[1]";
    }
    
    public static String linkedIncidentXpath() {
        return "//div[@id='control_75168AEA-F534-4B1D-A38A-FBA786BCF308']//li";
    }
    
    public static String otherXpath() {
        return "(//div[@id='control_E05537A2-F2F0-40F0-A0A0-1FBDD030BA37']//input)[1]";
    }
    
    public static String selectAllRiskSources() {
        return "//div[@id='control_05CA3F1F-B338-40ED-872B-9567C5ECB67B']//b[@original-title='Select all']";
    } 
     
     public static String submitButton() {
        return "//div[@id='control_0BACF2AC-5761-467A-B067-94EA3C08D423']";
    } 
     
     public static String searchButton()
    {
        return "//div[@id='btnActApplyFilter']";
    } 
     
    public static String recordToBeOpened()
    {
        return "(//div[@title='Global Company -> South Africa -> Victory Site'])[1]";
    }
    
     public static String supportingDocumentsTab()
    {
        return "//li[@id='tab_12BC3724-FC3C-458F-9348-D095B45903A7']";
    }   
    
    public static String uploadDocument()
    {
        return "//div[@id='control_5A3BDF3B-F8C5-446E-B512-901CF6EB2DF8']//b[@original-title='Upload a document']";
    }    
        
    public static String linkToADocument()
    {
        return "//div[@id='control_5A3BDF3B-F8C5-446E-B512-901CF6EB2DF8']//b[@original-title='Link to a document']";
    } 

    public static String LinkURL()
    {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }
       
      public static String urlTitle()
    {
        return "//div[@class='popup-container']//input[@id='urlTitle']";
    }
        
     public static String urlAddButton()
    {
        return "//div[@class='popup-container']//div[contains(text(),'Add')]";
    }
          
     public static String iframeName()
    {
        return "ifrMain";
    } 
     
    public static String riskReferenceDropDown()
    {
        return "//div[@id='control_5969E967-0B97-46D1-B1CE-12866D712A49']//li";
    } 
     
    public static String riskReferenceDropDownValue(String option)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + option + "']";
    } 
    
    
    
}
