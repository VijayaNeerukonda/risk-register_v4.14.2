/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.PageObjects;

import KeywordDrivenTestFramework.Core.BaseClass;

/**
 *
 * @author Vijaya
 */
public class FR2_FR10_RiskRegister_PageObjects extends BaseClass {

    public static String statusDropdownXpath() {
        return "//div[@id='control_87EEE5E5-7E0D-4E0B-A183-DD2E5613D0ED']//li";
    }
    
    public static String singleSelect(String text) {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + text + "'][1]";
    }
    
    
    public static String recordToBeOpened() {
        return "(//div[@title='0'])[1]";
    }
    
//     public static String recordToBeOpened(String text) {
//        return "//div[@id='control_3BDB074E-C37E-48F3-85C5-CDBE25069F9E']//tr//td//div[text()='" + text + "']";
//    }
    
    public static String saveButton(){
        return "//div[@id='btnSave_form_3AB45051-222E-416C-AAD3-86B2EDA98BED']";
    }
    
    public static String reviewRequestHistoryTab(){
        return "//li[@id='tab_B0C182CD-2A61-4C0A-BBE4-7EB7E2952B6B']";
    }
    
    public static String searchFilterButton(){
        return "//div[@id='control_98E51D6E-E82C-44ED-962D-3C1F1517AF1B']//div[@id='btnFilter']";
    }
    
    public static String searchButton(){
        return "//div[@id='btnActApplyFilter_ECA6353C-D43A-4E55-90F3-0FB6E11DA309_smc7']";
    }
    
    public static String recordTobeApproved(){
        return "//div[@id='control_98E51D6E-E82C-44ED-962D-3C1F1517AF1B']//div[@title='Annual review']";
    }
    
    public static String processflowXpath(){
        return "//div[@id='btnProcessFlow_form_ECA6353C-D43A-4E55-90F3-0FB6E11DA309']//span";
    }
    
    public static String approveCheckbox(){
        return "//div[@id='control_3B8C5E94-1428-46FF-AE02-65FC39962A77']//div[@class='c-chk']";
    }
    
    public static String historySaveButton(){
        return "//div[@id='btnSave_form_ECA6353C-D43A-4E55-90F3-0FB6E11DA309']";
    }
    
    public static String historyRecordNumber_xpath() {
        return "//div[@id='form_ECA6353C-D43A-4E55-90F3-0FB6E11DA309']//div[contains(text(),'- Record #')]";
    }
    
    public static String riskAssessmentAddButton() {
        return "//div[@id='control_3BDB074E-C37E-48F3-85C5-CDBE25069F9E']//div[@id='btnAddNew']";
    }
    
    public static String riskAssessmentProcessflow() {
        return "//div[@id='btnProcessFlow_form_3A80F1F4-AE90-47B2-B9BB-0C920589C04D']";
    }
    
    public static String riskSourceCheckbox() {
        return "//div[@id='control_0CA5B12B-E073-44E3-ADB4-324B4048410A']//div[@class='c-chk']";
    } 
    
    public static String riskSourceDropdown(){
        return "//div[@id='control_69C9B558-1EBF-4955-B9FC-5203F375AE72']//li";
    }
    
    public static String riskSourceDropdownExpand(String option, String index){
        return "(//div[contains(@class,'transition visible')]//li[@title='"+option+"']//i[@class='jstree-icon jstree-ocl'])["+index+"]";
    }
    
    public static String checklistSelect(String text){
        return "(//a[text()='" + text + "']/i[1])";
    }
    
    public static String riskSourceLabelXpath(){
        return "//div[@id='control_F29AD642-4B31-4DD7-9C70-EB3A4941FB3D']";
    }
            
    public static String riskCheckbox() {
        return "//div[@id='control_0DFD574B-9DB4-474A-B6F3-9274F0824C06']//div[@class='c-chk']";
    }
    
    public static String riskDropdown(){
        return "//div[@id='control_B5A8D870-64A7-497A-96C7-C4E137C2FE77']//li";
    }
    
    public static String riskLabel(){
        return "//div[@id='control_6CE93323-A05B-41F3-BC26-B62601C7C9DB']";
    }
    
    public static String riskDescription(){
        return "//div[@id='control_8C191992-EC11-43A5-B2E9-2AD727C55B1C']//textarea";
    }
    
    public static String laymansTermsCauses(){
        return "//div[@id='control_BC9C0B8E-87F9-46D5-8C1D-47913BA15CF2']//textarea";
    }
    
    public static String penaltiesConsequences(){
        return "//div[@id='control_C3C9D8CC-E75D-4552-A0DE-F61406752A2A']//textarea";
    }
    
    public static String activitySelectAll(){
        return "//div[@id='control_E8A76AE4-9F72-45E7-9C36-E9B46283CC74']//b[@original-title='Select All']";
    }
    
    public static String riskOwner(){
        return "//div[@id='control_C57075CF-1363-4C90-BE35-4D0785DB5FED']//li";
    }
    
    public static String bowtieReferenceNumber(){
        return "//div[@id='control_BE184545-4185-465A-995F-353C0F04C7C1']//li";
    }
    
    public static String saveRiskAssessmentButton(){
        return "//div[@id='btnSave_form_3A80F1F4-AE90-47B2-B9BB-0C920589C04D']";
    }
    
    public static String riskAssessmentRecordNumber(){
        return "//div[@id='form_3A80F1F4-AE90-47B2-B9BB-0C920589C04D']//div[contains(text(),'- Record #')]";
    }
    
    public static String supportingInfoPanel(){
        return "//span[text()='Supporting Information: Impact']";
    }
    
    public static String controlsPanel(){
        return "//span[text()='Controls']";
    }
    
    public static String linkedStrategicObjectivesPanel(){
        return "//span[text()='Linked Strategic Objectives']";
    }
    
    public static String linkedComplianceRequirementsPanel(){
        return "//span[text()='Linked Compliance Requirements']";
    }
    
    public static String linkedKeyRiskIndicatorsPanel(){
        return "//span[text()='Linked Key Risk Indicators']";
    }
    
    public static String suppDocsPanel(){
        return "//span[text()='Supporting Documents']";
    }
    
    
            
    public static String controlsRecordsReplicate(){
        return "//div[@id='control_F9ED589E-4CB3-4309-BBDF-686663B42506']//div[contains(text(),'Drag a column header and drop it here to group by that column')]/../div[3]//tr[1]";
    }
    
    
    public static String additionalInformationCheckbox(){
        return "//div[@id='control_FD069FC0-2F4F-4BA0-894D-F4678C8BE81B']//div[@class='c-chk']";
    }
    
    public static String saveChevronRiskAssessmentButton(){
        return "//div[@id='btnSave_form_3A80F1F4-AE90-47B2-B9BB-0C920589C04D']//div";
    }
    
    public static String saveAndCloseRiskAssessmentButton(){
        return "//div[@id='btnSaveClose_form_3A80F1F4-AE90-47B2-B9BB-0C920589C04D']";
    }
    
    public static String riskAssessmentCloseCross(){
        return "(//div[@id='form_3A80F1F4-AE90-47B2-B9BB-0C920589C04D']//i[@class='close icon cross'])[1]";
    } 
    
    
    public static String addNewButton(){
        return "//div[text()='Add new']";
    }
    
    public static String riskLibraryProcessflow(){
        return "//div[@id='btnProcessFlow_form_75490176-392D-464D-812B-E3F9E9AC942D']";
    }
    
    public static String riskLibraryRisk(){
        return "(//div[@id='control_38779557-7763-43A8-A0EF-A5BC98AAF5E7']//input)[1]";
    }
    
    public static String riskLibraryImpactType() {
        return "//div[@id='control_3B7B1EE5-F549-4C34-853A-C8218D87BC29']//li";
    }
    
     public static String riskLibraryImpactTypeSelectAll() {
        return "//div[@id='control_3B7B1EE5-F549-4C34-853A-C8218D87BC29']//b[@original-title='Select All']";
    }
    
    public static String riskLibraryPUEDropdown() {
        return "//div[@id='control_D33817E8-42C9-4AE4-B2D1-00ADD1A8FBE4']//li";
    }
    
    public static String relatedCriticalControlDropdown() {
        return "//div[@id='control_48E40813-477B-4D8F-968E-0012A53596BC']//li";
    } 
    
    public static String riskLibrarySave() {
        return "//div[@id='btnSave_form_75490176-392D-464D-812B-E3F9E9AC942D']";
    } 
    
    public static String relatedRiskSourceCheckbox(String text) {
        return "(//a[text()='" + text + "']/i[1])";
    }
    
    public static String riskAssessmentClose() {
        return "(//div[@id='form_3A80F1F4-AE90-47B2-B9BB-0C920589C04D']//i[@class='close icon cross'])[1]";
    }
    
    public static String expandChevronRelatedRisk(String option, String index){
        return "(//div[contains(@class,'transition visible')]//li[@title='"+option+"']//i[@class='jstree-icon jstree-ocl'])["+index+"]";
    }
    
    //FR4
    public static String causesPanel(){
        return "//span[text()='Causes']";
    }
    
    public static String consequencesPanel(){
        return "//span[text()='Consequences']";
    }
    
    public static String likelihoodLabel() {
        return "//div[@id='control_A5151DDF-E3A7-4FA8-81E5-82581080C827']";
    }
    
    public static String likelihoodDropdown() {
        return "//div[@id='control_41FDFEB0-3496-480C-A772-548B83E7F732']//li";
    }
     
    public static String viewBowtieButton() {
        return "//div[@id='control_736C3211-2E44-458F-8450-DF7743349CA7']";
    }
    
    //FR5
    public static String causesAddButton() {
        return "//div[@id='control_0EF7C156-B478-40BC-B29D-A9643890CEA1']//div[@id='btnAddNew']";
    }
     
    public static String causesProcessflow() {
        return "//div[@id='btnProcessFlow_form_14178C6A-02A3-43F7-8513-013C56F2A71F']";
    }
    
    public static String causeTitle(){
        return "(//div[@id='control_F1AF5A3C-AF56-4016-8F74-5BC1DAA9B5FA']//input)[1]";
    }
    
    public static String causeField(){
        return "(//div[@id='control_16ADAF90-6D9A-403B-BA0C-F8D41353A9E2']//input)[1]";
    }
        
    public static String controlsDropdown() {
        return "//div[@id='control_62BE2AB9-0A72-4BD6-ACF5-F39A2328BE2B']//li";
    }
    
    public static String controlsDropdownExpand(String option, String index){
        return "(//div[contains(@class,'transition visible')]//li[@title='"+option+"']//i[@class='jstree-icon jstree-ocl'])["+index+"]";
    }
    
    public static String controlsSaveButton() {
        return "//div[@id='btnSave_form_14178C6A-02A3-43F7-8513-013C56F2A71F']";
    }
     
    public static String causesRecordNumber(){
        return "(//div[@id='form_14178C6A-02A3-43F7-8513-013C56F2A71F']//div[contains(text(),'- Record #')])[1]";
    }
    
    public static String preventiveControlsReplication(){
        return "//div[@id='control_8AB00884-981A-40FB-A6C4-A4F2D21DF6F8']//div[contains(text(),'Drag a column header and drop it here to group by that column')]/../div[3]//tr[1]";
    }
    
    //UC_RSM_05_02
    public static String preventiveControlsAddButton() {
        return "//div[@id='control_8AB00884-981A-40FB-A6C4-A4F2D21DF6F8']//div[@id='btnAddNew']";
    }
    
    public static String preventiveControlsProcessflow() {
        return "//div[@id='btnProcessFlow_form_780ECF4D-5E78-496C-80FD-24A4D434003C']";
    }
    
    public static String preventiveControlsSaveButton() {
        return "//div[@id='btnSave_form_780ECF4D-5E78-496C-80FD-24A4D434003C']";
    } 
    
    public static String preventiveControlsRecordNumber(){
        return "(//div[@id='form_780ECF4D-5E78-496C-80FD-24A4D434003C']//div[contains(text(),'- Record #')][1])[1]";
    }  
    
    //FR6
    public static String consequencesAddButton() {
        return "//div[@id='control_57AAB68A-F12A-47EA-881C-1B3161447A2D']//div[@id='btnAddNew']";
    }
    
    public static String consequencesProcessflow() {
        return "//div[@id='btnProcessFlow_form_0250404C-8496-4ECE-B1DF-1518B463D73F']";
    }
    
    public static String consequenceTypeDropdown() {
        return "//div[@id='control_73614700-2430-4F0D-BFFF-F15D45A7D142']//li";
    }
    
    public static String maxConsequenceDropdown() {
        return "//div[@id='control_17441320-13C0-4D03-BFB5-8C7FCB9A1CEE']//li";
    }
    
    public static String consequenceDropdown() {
        return "//div[@id='control_EFF36E2D-78CD-4BFE-AC8D-D39910BA61A4']//li";
    }
      
    public static String consequencesTextbox() {
        return "//div[@id='control_5858AB70-A475-47D2-A794-155CFC765C37']//textarea";
    }
     
    public static String consqControlsDropdown() {
        return "//div[@id='control_69750100-1F02-49CC-9F70-1BAAAD7D9FA4']//li";
    }
    
    public static String consequencesSaveButton() {
        return "//div[@id='btnSave_form_0250404C-8496-4ECE-B1DF-1518B463D73F']";
    }
    
    public static String consequencesRecordNumber(){
        return "(//div[@id='form_0250404C-8496-4ECE-B1DF-1518B463D73F']//div[contains(text(),'- Record #')][1])[1]";
    }
    
    public static String mitigationControlsReplication(){
        return "//div[@id='control_D4D30DB4-E22E-43AE-8955-88C43147D350']//div[contains(text(),'Drag a column header and drop it here to group by that column')]/../div[3]//tr[1]";
    }
    
    //UC_RSM_06_02
    public static String mitigationControlsAddButton() {
        return "//div[@id='control_D4D30DB4-E22E-43AE-8955-88C43147D350']//div[@id='btnAddNew']";
    }
    
    public static String mitigationControlsProcessflow() {
        return "//div[@id='btnProcessFlow_form_FC6886BC-597B-4F7F-A41D-F1560DB4A642']";
    }
    
    public static String mitigationControlsSaveButton() {
        return "//div[@id='btnSave_form_FC6886BC-597B-4F7F-A41D-F1560DB4A642']";
    }
    
    public static String mitigationControlsRecordNumber(){
        return "(//div[@id='form_FC6886BC-597B-4F7F-A41D-F1560DB4A642']//div[contains(text(),'- Record #')][1])[1]";
    }
    
    
    
            
    //FR8
    public static String inherentRiskPanel() {
        return "//span[text()='Inherent Risk']";
    }
    
    public static String impactInherentRiskAddButton() {
        return "//div[@id='control_5E306D7C-7386-49A6-B5E8-18889F943C96']//div[@id='btnAddNew']";
    }
    
    public static String inherentRiskProcessflow() {
        return "//div[@id='btnProcessFlow_form_D30529CB-0238-4903-8C8E-07B2D7EE723F']";
    }
    
    public static String consequenceTypeIIRDropdown() {
        return "//div[@id='control_ECDA14DC-13FD-4511-B373-23EEB01C7F01']//li";
    }
    
    public static String singleSelectIndex(String text, String index) {
        return "(//div[contains(@class, 'transition visible')]//a[text()='" + text + "'])["+index+"]";
    }
    
    public static String consequenceIIRDropdown() {
        return "//div[@id='control_002897B8-C514-43A0-8A4D-AB34EBE5AC2F']//li";
    }
    
    public static String consequenceDropdownExpand(String option, String index){
        return "(//div[contains(@class,'transition visible')]//li[@title='"+option+"']//i[@class='jstree-icon jstree-ocl'])["+index+"]";
    } 
     
    public static String consequenceDropdownExpand1(String option, String index){
        return "(//div[contains(@class,'transition visible')]//li[contains(@title,'"+option+"')]//i[@class='jstree-icon jstree-ocl'])[9]";
    }
    
    public static String environment(String index){
        return "(//div[contains(@class,'transition visible')]//li[@title='Environment ']//i[@class='jstree-icon jstree-ocl'])["+index+"]";
    } 
     
    public static String likelihoodRatingInherentRisk() {
        return "(//div[@id='control_2EBB8D75-E591-409C-B0E0-719BC842BA8E']//li)[1]";
    } 
     
    public static String impactInherentRiskSaveButton() {
        return "//div[@id='btnSave_form_3A80F1F4-AE90-47B2-B9BB-0C920589C04D']";
    } 
     
    public static String impactInherentRiskRecordNumber(){
        return "(//div[@id='form_D30529CB-0238-4903-8C8E-07B2D7EE723F']//div[contains(text(),'- Record #')])[1]";
    }  
    
    public static String impactInherentRiskCloseButton(){
        return "(//div[@id='form_D30529CB-0238-4903-8C8E-07B2D7EE723F']//i[@class='close icon cross'])[1]";
    }  
    
    public static String impactInherentRiskRecordToBeOpened(){
        return "//div[@id='control_5E306D7C-7386-49A6-B5E8-18889F943C96']//div[contains(text(),'Drag a column header and drop it here to group by that column')]/../div[3]//tr[1]";
    }  
     
             
    public static String residualRiskSearchButton() {
        return "//div[@id='control_BFBB2EF8-8044-42EF-8FFD-A17BD4D7D7E1']//div[@id='btnFilter']";
    } 
    
    public static String residualRiskSearchButton2() {
        return "(//div[@id='control_BFBB2EF8-8044-42EF-8FFD-A17BD4D7D7E1']//div[@title='Search'])[2]";
    } 
    
    public static String impactResidualRiskGrid() {
        return "//div[@id='control_BFBB2EF8-8044-42EF-8FFD-A17BD4D7D7E1']//div[text()='Impact - Residual Risk']";
    }
    
    public static String impactRiskRecordsReplicate(){
        return "//div[@id='control_BFBB2EF8-8044-42EF-8FFD-A17BD4D7D7E1']//div[contains(text(),'Drag a column header and drop it here to group by that column')]/../div[3]//tr[1]";
    }
    
    
    
    //FR9
    public static String controlsAssessmentPanel() {
        return "//span[text()='Step 2: Controls Assessment']";
    }
    
    public static String riskAssessmentControlsAddButton() {
        return "//div[@id='control_F9ED589E-4CB3-4309-BBDF-686663B42506']//div[@id='btnAddNew']";
    }
    
    public static String controlAssessmentProcessflow() {
        return "//div[@id='btnProcessFlow_form_A21DE592-B894-4569-8EC5-0666495DF708']";
    }
    
    
    public static String controlCategoryDropdown() {
        return "//div[@id='control_B5DF4C13-9442-46D9-8FF0-C7E70CF2E7DC']//li";
    }
    
     public static String controlCategoryDropdownExpand(String option, String index){
        return "(//div[contains(@class,'transition visible')]//li[@title='"+option+"']//i[@class='jstree-icon jstree-ocl'])["+index+"]";
    } 
         
     public static String criticalCheckbox() {
        return "//div[@id='control_0C3E4AB7-2E09-4EEA-9663-BDDD8D0380B9']/div[1]";
    }
    
    public static String controlDescription(){
        return "(//div[@id='control_6754FD09-D78D-4DE7-A588-FDDFDCCC78F5']//input)[1]";
    } 
     
    public static String controlOwnerDropdown() {
        return "//div[@id='control_7D9D2EE5-75F6-4F44-8896-C03AFF83F7F7']//li";
    }
    
    public static String groupSiteDropdown() {
        return "//div[@id='control_BA5A4DC9-D2A3-4E1D-86E8-72B443FFB968']//li";
    }
    
    public static String controlImplementationDropdown() {
        return "//div[@id='control_6A6FD9CA-8F86-4108-B9FF-E64096C8C47C']//li";
    }
    
    public static String controlQualityDropdown() {
        return "//div[@id='control_EE4E7BE4-1899-40AD-B337-94621B1ABEDF']//li";
    }
    
    public static String monitoringFrequencyDropdown() {
        return "//div[@id='control_214D0A33-0379-433F-B6DC-9CC15411B0C5']//li";
    }
    
    public static String monitoringOwnerDropdown() {
        return "//div[@id='control_5A412952-2785-479F-8431-EC562A6A2A57']//li";
    }
    
    public static String riskAssessmentControlsSaveButton() {
        return "//div[@id='btnSave_form_A21DE592-B894-4569-8EC5-0666495DF708']";
    }
    
    public static String riskAssessmentControlsRecordNumber(){
        return "(//div[@id='form_A21DE592-B894-4569-8EC5-0666495DF708']//div[contains(text(),'- Record #')][1])[1]";
    }  
    
//    public static String riskAssessmentControlsRecordNumber(){
//        return "//tr[@id='formWrapper_0']//span[@type='recordnumber']";
//    }  
   
    //FR10
    public static String residualRiskPanel(){
        return "//span[text()='Residual Risk']";
    }
    
    public static String refreshButton(){
        return "//div[@id='control_28D9B3A5-79E8-4D94-AA07-2F3B9C331BD0']";
    }
    
    public static String residualRiskLikelihoodRatingDropdown(){
        return "(//div[@id='control_2EBB8D75-E591-409C-B0E0-719BC842BA8E']//li)[2]";
    }
    
    public static String residualRiskSaveButton(){
        return "//div[@id='btnSave_form_4F422593-AE77-4F9F-8669-EC7F6BCEEBFF']";
    }
    
    public static String impactResidualRiskCloseButton(){
        return "(//div[@id='form_4F422593-AE77-4F9F-8669-EC7F6BCEEBFF']//i[@class='close icon cross'])[1]";
    } 
    
    public static String residualRatingLabel(){
        return "//div[@id='control_22A2049A-08BE-4F6F-8559-2505FFEBE016']";
    }
    
            
    public static String residualRiskRecordNumber(){
        return "(//div[@id='form_4F422593-AE77-4F9F-8669-EC7F6BCEEBFF']//div[contains(text(),'- Record #')][1])[1]";
    } 
    
    public static String residualRiskRefreshButton(){
        return "//div[@id='control_28D9B3A5-79E8-4D94-AA07-2F3B9C331BD0']";
    }
    
    
    //FR11
    public static String relatedIncidentsPanel(){
        return "//span[text()='Related Incidents']";
    }
    
    public static String incidentManagementRecordOpen(){
        return "//div[@id='control_2483B06B-DC43-4D02-82A9-63ED91F492D7']/div[1]/div[2]/div[2]/div[1]/div[3]//tr[1]";
    }
    
    public static String incidentManagementProcessflow(){
        return "//div[@id='btnProcessFlow_form_B6196CB4-4610-463D-9D54-7B18E614025F']";
    }
    
    //FR12
    public static String relatedFindingsPanel(){
        return "//span[text()='Related Findings']";
    }
    
    public static String findingsRecordOpen(){
        return "//div[@id='control_027E3C01-EB36-467F-8D41-2B2F1B4519DC']/div[1]/div[2]/div[2]/div[1]/div[3]//tr[1]";
    }
    
    public static String findingsRecordOpenProcessflow(){
        return "//div[@id='btnProcessFlow_form_85E1FC8B-87EB-4A2B-B406-AC00B5214FE5']";
    }
    
    
    //FR13
    public static String actionsPanel(){
        return "//span[text()='Actions']";
    }
    
    public static String integratedRiskActionsAddButton() {
        return "//div[@id='control_D7C55DBE-414D-424D-977B-EA4F4D8072E0']//div[@id='btnAddNew']";
    }
    
    public static String integratedRiskActionsProcessflowButton() {
        return "//div[@id='btnProcessFlow_form_C4C32519-76B9-4ED9-8EDA-2318F0BCDDEC']";
    }
    
    public static String actionDescription() {
        return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']//textarea";
    }
    
    public static String departmentResponsible() {
        return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']//li";
    }
    
    public static String responsiblePerson() {
        return "//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']//li";
    }
    
    public static String actiondueDateXpath() {
        return "//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']//input";
    }
    
    public static String integratedRiskActionsSaveButton() {
        return "//div[@id='btnSave_form_C4C32519-76B9-4ED9-8EDA-2318F0BCDDEC']";
    }
    
    public static String integratedRiskActionsRecordNumber(){
        return "//tr[@id='form_C4C32519-76B9-4ED9-8EDA-2318F0BCDDEC']//span[@type='recordnumber']";
    } 
    
    
    //FR14
    public static String searchButtonOnSearchpage()
    {
    return "//div[@id='btnActApplyFilter']";
    }
    
    
    public static String reportsButton()
    {
    return "//div[@id='btnReports']";
    }

    public static String viewReport()
    {
    return "//span[@title='View report ']";
    }

    public static String continueButton()
    {
    return "//div[@id='btnConfirmYes']";
    }

    public static String exportDropdownMenu()
    {
    return "//img[@id='viewer_ctl05_ctl04_ctl00_ButtonImgDown']";
    }

    public static String exportWord()
    {
    return "//a[@title='Word']";
    }

    public static String fullReport()
    {
    return "//span[@title='Full report ']";
    }
    
    //FR7
    public static String openRiskRecord()
    {
        return "//span[@title='190']";
    }
    
    public static String openRiskAssessmentRecord()
    {
        return "((//div[@id='control_3BDB074E-C37E-48F3-85C5-CDBE25069F9E']//div//table)[4]//tr)[1]";
    }
    
    public static String View_Bowtie()
    {
        return "//div[@id='control_736C3211-2E44-458F-8450-DF7743349CA7']";
    }

    public static String moveToElement() {
        return "(//div[@id='bowTie']//div[@data-type='event']//div[@class='title'])";
    }
    
    public static String addConsequencePlusIcon() {
        return "(//div[@id='bowTie']/../..//a[@class='item add bRight']//i)[1]";
    }
    
    public static String diagramConsequenceTypeDropdown() {
        return "//div[@id='consequence_1']//div[@class='titleBar']";
    }
    
    public static String clickToEditTextfield(String index) {
        return "(//div[@data-ph='Click to edit...'])["+index+"]";
    }
    
    public static String diagramMaxConsequencesDropdown() {
        return "(//div[@original-title='Consequence'])[2]";
    }
    
    public static String diagramSaveButton()
    {
        return "//div[@id='btnSave']";
    }
    
            
    public static String flipBowtie()
    {
        return "//div[@id='btnFlip']";
    }

    
    public static String Print()
    {
        return "//div[@title='Print']";
    }

    public static String Print_Button()
    {
        return "//div[@id='btnPrint']";
    }

    public static String Nav_Back()
    {
        return "//div[@id='bowtieNav']//i[@class='back icon arrow-left']";
    }

    public static String Add_Cause_Button()
    {
        return "//div[@original-title='Cause']/../..//a[@class='item add bLeft']";
    }

    public static String Cause_Wording()
    {
        return "//div[@original-title='Cause']";
    }

    public static String PleaseSelectControlCategory_Dropdown()
    {
        return "//div[@class='block pControl jtk-endpoint-anchor jtk-connected']//div[@title='Please select']";
    }

    public static String Consequence_Block()
    {
        return "//div[@original-title='Consequence']";
    }

    public static String viewBowtie_SaveButton()
    {
        return "//div[@id='act_bowtie_right']//div[text()='Save']";
    }

    public static String Control_Description()
    {
        return "//div[text()='...']";
    }

    public static String Save_Button()
    {
        return "//div[@id='btnSave']";
    }

    public static String Control__Description()
    {
        return "//div[@title='Please select']/../..//div[@class='content']";
    }

    public static String Delete_Cause_Button()
    {
        return "//div[@title='Cause']/../..//a[@class='item delete']";
    }

    public static String Yes_Button()
    {
        return "//div[@class='confirm-popup popup']//div[text()='Yes']";
    }

    public static String Ok_Button()
    {
        return "//div[text()='Record has successfully been deleted']/..//div[@id='btnHideAlert']";
    }

    public static String Consequence_Wording()
    {
        return "//div[@title='Consequence']";
    }

    public static String Delete_Consequence_Button()
    {
        return "//div[@title='Consequence']/../..//a[@class='item delete']";
    }

    public static String Event_Left_Plus_Button()
    {
        return "//div[@title='Risk']/../..//a[@class='item add bLeft']";
    }

    public static String Cause_Description()
    {
        return "(//div[@title='Cause']/../..//div[@class='content'])[2]";
    }

    public static String Event_Block()
    {
        return "//div[@title='Risk']";
    }

    public static String Cause__Description()
    {
        return "//div[@title='Please select']/../..//div[@class='content']";
    }

    public static String Flip_Button()
    {
        return "//div[@id='btnFlip']";
    }

    public static String Event_Right_Plus_Button()
    {
        return "//div[@title='Risk']/../..//a[@class='item add bRight']";
    }

    public static String Consequence_Description()
    {
        return "(//div[@title='Consequence']/../..//div[@class='content'])[2]";
    }

    public static String Print_Frame()
    {
        return "//html[@class='']//print-preview-app";
    }
    
    public static String printPreview()
    {
        return "//print-preview-app";
    }

  
    
    
    
}
