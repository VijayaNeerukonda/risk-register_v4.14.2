/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Utilities;

import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.WinDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.currentBrowser;
import static KeywordDrivenTestFramework.Core.BaseClass.relativeScreenShotPath;
import static KeywordDrivenTestFramework.Core.BaseClass.setScreenshotPath;
import static KeywordDrivenTestFramework.Core.BaseClass.testCaseId;
import KeywordDrivenTestFramework.Entities.AppTable.AppTable;
import KeywordDrivenTestFramework.Entities.AppTable.AppTableColumn;
import KeywordDrivenTestFramework.Entities.AppTable.AppTableRow;
import KeywordDrivenTestFramework.Reporting.Narrator;
import static KeywordDrivenTestFramework.Utilities.AppiumDriverUtility.Driver;
import com.google.common.base.Stopwatch;
import io.appium.java_client.windows.WindowsDriver;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author Steve
 */
public class WindowsDriverUtility
{

    public WindowsDriver Driver = null;
    private Runtime server;
    
    public WindowsDriverUtility(String app)
    {
        LaunchServer();
        Driver = setup(app);

        if (Driver == null)
        {
            Narrator.logError("Failed to create Windows Driver!");
            throw new NullPointerException("Failed to create Windows driver!");
        }
    }

    public Boolean LaunchServer()
    {
        try
        {
            server = Runtime.getRuntime();

            String command = "cmd.exe /c start cmd.exe /k \"C:\\Program Files (x86)\\Windows Application Driver\\WinAppDriver.exe\"";
            server.exec(command);
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to launch windows driver server, exception - " + e.getMessage());
            return false;
        }
    }

    public WindowsDriver setup(String AppId)
    {
        try
        {
            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setCapability("app", AppId);
            capabilities.setCapability("deviceName", "WindowsPC");
            capabilities.setCapability("platformName", "Windows");
            capabilities.setCapability("automationName","UiAutomator2");

            return new WindowsDriver(new URL("http://127.0.0.1:4723"), capabilities);
        }
        catch (Exception e)
        {
            Narrator.logError("Error setting Capabilities - " + e.getMessage());
            return null;
        }
    }
    
    public boolean closeApp()
    {
        try
        {
            Driver.closeApp();          
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }

    public boolean shutDown()
    {
        try
        {
            Driver.quit();
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }

    public boolean enterTextByXpath(String elementXpath, String textToEnter)
    {
        try
        {

            this.waitForElementByXpath(elementXpath);
            WebElement elementToTypeIn = Driver.findElement(By.xpath(elementXpath));
            elementToTypeIn.clear();
            elementToTypeIn.sendKeys(textToEnter);
            Narrator.logDebug("Entered Text of: " + textToEnter + " to: " + elementXpath);
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" entering text - " + elementXpath + " - " + e.getMessage());

            return false;
        }
    }
    
    public void prompt()
    {
        Set<String> allWindowHandles = WinDriverInstance.Driver.getWindowHandles();
        
        while(allWindowHandles.size() < 1)
        {
            try
            {
                 Thread.sleep(500);
            }
            catch(Exception e)
            {}
           
            allWindowHandles = WinDriverInstance.Driver.getWindowHandles();
        }
        
        List<String> t = new ArrayList<String>(allWindowHandles);
        
        
        WinDriverInstance.Driver.switchTo().window(t.get(0));
    }
    
    
    

    public AppTable createTable(String tablePath)
    {
        Stopwatch sw = Stopwatch.createStarted();
        
        
        
        LinkedList<AppTableRow> Table = new LinkedList<>();
        waitForElementByXpath(tablePath);
        try
        {
            String rowPath = ".//*[@Name='Data Panel']/*";
            String dataPath = ".//*";
            String allData = tablePath + "//*[@Name='Data Panel']/*//*";
            //Get all elements
            List<WebElement> rows = Driver.findElements(By.xpath(rowPath));
            //iterates through rows
            
            List<WebElement> allItems = Driver.findElements(By.xpath(allData));
            
            int colCount = rows.get(0).findElements(By.xpath(dataPath)).size() - 1;
            
           
            
            for (int i = 0; i < allItems.size(); i += colCount)
            {
                AppTableRow newRow = new AppTableRow();
                
                for (int j = 0; j < colCount; j++)
                {
                    String column = allItems.get(i+j).getAttribute("Name");
                    column = column.replaceAll("([row \\d+])\\w+","");
                    
                    String text = allItems.get(i+j).getText();

                    AppTableColumn newCol = new AppTableColumn(column, text, allItems.get(i+j));
                    newRow.DataColumns.add(newCol);
                }
                
                 Table.add(newRow);
            }
                      
             sw.stop();
            long seconds = sw.elapsed(TimeUnit.SECONDS);
         
            
            return new AppTable(Table);
        }
        catch (Exception e)
        {
            return null;
        }

    }

    public boolean clickElementInTable(String tablePath, String ColumnToClick, String rowIdentfier)
    {
        try
        {
            this.waitForElementByXpath(tablePath);
            WebElement tableElement = Driver.findElement(By.xpath(tablePath));

            List<WebElement> rows = tableElement.findElements(By.xpath(".//*[contains(@Name,'Row')]//*"));

            for (WebElement item : rows)
            {

                String text = item.getText();

                if (text.equals(rowIdentfier))
                {
                    String name = item.getAttribute("Name");
                    String regex = "\\d+";

                    Pattern pattern = Pattern.compile(regex);
                    Matcher matcher = pattern.matcher(name);
                    matcher.find();
                    String num = matcher.group(0);

                    String build = tablePath + "//*[contains(@Name,'" + ColumnToClick + " row " + num + "')]";
                    if (clickElementbyXpath(build))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            }

            return false;
        }
        catch (Exception e)
        {
            return false;
        }
    }
    
     public boolean clickWebElement(WebElement element)
    {
        try
        {
            
            WebElement elementToClick = element;
            elementToClick.click();
            Narrator.logDebug("Clicked element : " + element);
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" Failed to click on element by Xpath - " + e.getMessage());

            return false;
        }
    }

    public boolean clickElementbyXpath(String elementXpath)
    {
        try
        {
            prompt();
            WebElement elementToClick = returnElementByXpath(elementXpath);
            elementToClick.click();
            Narrator.logDebug("Clicked element by Xpath : " + elementXpath);
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" Failed to click on element by Xpath - " + e.getMessage());

            return false;
        }
    }
    
    
    public boolean clickElementbyName(String elementXpath)
    {
        try
        {
            prompt();
            WebElement elementToClick = returnElementByName(elementXpath);
            elementToClick.click();
            Narrator.logDebug("Clicked element by Xpath : " + elementXpath);
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" Failed to click on element by Xpath - " + e.getMessage());

            return false;
        }
    }
    
    
    public boolean waitForElementTextValue(String xpath, String expectedValue)
    {
        int counter = 0;
        try
        {
            WebElement element = returnElementByXpath(xpath);
            
            while(!element.getText().equals(expectedValue) && counter < 60)
            {
                String debug = element.getText();
                pause(500);
                counter ++;
            }
            
            if(element.getText().equals(expectedValue))
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }
        catch(Exception e)
        {
            return false;
        }
    }
    
     public boolean waitForElementTextValue(String xpath, String expectedValue, int duration)
    {
        int counter = 0;
        try
        {
            WebElement element = returnElementByXpath(xpath);
            duration = duration *2;
            while(!element.getText().equals(expectedValue) && counter < duration)
            {
                String debug = element.getText();
                pause(500);
                counter ++;
            }
            
            if(element.getText().equals(expectedValue))
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }
        catch(Exception e)
        {
            return false;
        }
    }
    
     public void takeScreenShot(String screenShotDescription, boolean isError)
    {
        String imageFilePathString = "";

        if (testCaseId == null)
        {
            return;
        }

        if (this.Driver == null)
        {
            return;
        }

        try
        {
            StringBuilder imageFilePathBuilder = new StringBuilder();
            // add date time folder and test case id folder
            imageFilePathBuilder.append(AppiumDriverUtility.reportDirectory + "\\");
            // + testCaseId + "\\"          
                       

            relativeScreenShotPath = testCaseId + "\\";
            
            
            String PorF="";
            if (isError)
            {
//                imageFilePathBuilder.append("FAILED_");
                PorF = "FAILED_";
            }
            else
            {
//                imageFilePathBuilder.append("PASSED_");
                PorF = "PASSED_";
            }
            
            

            relativeScreenShotPath +=PorF+ testCaseId + "_" + screenShotDescription + ".png";

            imageFilePathBuilder.append(relativeScreenShotPath);

//            if (imageFilePathBuilder.toString().contains("FAILED_"))
//            {
//                relativeScreenShotPath = "FAILED_" + relativeScreenShotPath;
//            }
//            else if (imageFilePathBuilder.toString().contains("PASSED_"))
//            {
//                relativeScreenShotPath = "PASSED_" + relativeScreenShotPath;
//            }

            //imageFilePathBuilder.append(this.generateDateTimeString() + ".png");
            imageFilePathString = imageFilePathBuilder.toString();
            
            String projectPath = System.getProperty("user.dir");
            
            setScreenshotPath(imageFilePathString);

            if (this.Driver != null)
            {
                prompt();
                
                File screenShot = ((TakesScreenshot) this.Driver).getScreenshotAs(OutputType.FILE);
                
//                if (currentBrowser.equals(currentBrowser.mobileSafari))
//                {
//                    BufferedImage img = ImageIO.read(screenShot);
//                    BufferedImage dest = img.getSubimage(0,100,640,780);
//
//                    ImageIO.write(dest,"png",screenShot);
//                }            
                
                FileUtils.copyFile(screenShot, new File(imageFilePathString.toString()));
            }
        }
        catch (WebDriverException | IOException e)
        {
            Narrator.logError("[Error] could not take screenshot - " + imageFilePathString.toString() + " - " + e.getMessage());
        }
    }
    
    public void pause(int milisecondsToWait)
    {
        try
        {
            Thread.sleep(milisecondsToWait);
        }
        catch (Exception e)
        {

        }
    }
    
    public WebElement returnElementByXpath(String xpath)
    {
        boolean elementFound = false;
        WebElement element;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < 30)
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);

                    wait.until(drv -> drv.findElement(By.xpath(xpath)));                   
                    element = Driver.findElement(By.xpath(xpath));
                    
                    return element;
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                //Thread.sleep(500);
                waitCount++;
            }
            if (waitCount == ApplicationConfig.WaitTimeout())
            {
                Narrator.logError("Reached TimeOut whilst waiting for element by Xpath '" + xpath + "'");

                return null;
            }

        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by Xpath '" + xpath + "' - " + e.getMessage());

        }
        return null;
    }
    
    
     public WebElement returnElementByName(String name)
    {
        boolean elementFound = false;
        WebElement element;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < 30)
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);

                    wait.until(drv -> drv.findElement(By.name(name)));                   
                    element = Driver.findElement(By.name(name));
                    
                    return element;
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                //Thread.sleep(500);
                waitCount++;
            }
            if (waitCount == ApplicationConfig.WaitTimeout())
            {
                Narrator.logError("Reached TimeOut whilst waiting for element by Xpath '" + name + "'");

                return null;
            }

        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by Xpath '" + name + "' - " + e.getMessage());

        }
        return null;
    }

    public boolean waitForElementByXpath(String elementXpath)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < 30)
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                   
                    if ( wait.until(drv -> drv.findElement(By.xpath(elementXpath)).isDisplayed()) != null)
                    {
                        elementFound = true;
                        Narrator.logDebug(" Found element by Xpath : " + elementXpath);
                        break;
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                //Thread.sleep(500);
                waitCount++;
            }
            if (waitCount == ApplicationConfig.WaitTimeout())
            {
                Narrator.logError("Reached TimeOut whilst waiting for element by Xpath '" + elementXpath + "'");

                return elementFound;
            }

        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by Xpath '" + elementXpath + "' - " + e.getMessage());

        }
        return elementFound;
    }

}
