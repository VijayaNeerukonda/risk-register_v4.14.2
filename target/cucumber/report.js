$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Sample/SugarCRM1.feature");
formatter.feature({
  "line": 1,
  "name": "SugarCRM Feature",
  "description": "",
  "id": "sugarcrm-feature",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 8,
  "name": "Create Contacts",
  "description": "",
  "id": "sugarcrm-feature;create-contacts",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 7,
      "name": "@CreateContact"
    }
  ]
});
formatter.step({
  "line": 9,
  "name": "I navigate to the Contacts page",
  "keyword": "Given "
});
formatter.step({
  "line": 10,
  "name": "I create a contact with first name \u003cfirstname\u003e and last name \u003clastname\u003e and email \u0027\u003cemail\u003e\u0027",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "I should see the contact with first name \u003cfirstname\u003e \u003clastname\u003e is created",
  "keyword": "Then "
});
formatter.step({
  "line": 12,
  "name": "I logout of SugarCRM",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "I should see login page",
  "keyword": "Then "
});
formatter.examples({
  "line": 15,
  "name": "",
  "description": "",
  "id": "sugarcrm-feature;create-contacts;",
  "rows": [
    {
      "cells": [
        "firstname",
        "lastname",
        "email"
      ],
      "line": 16,
      "id": "sugarcrm-feature;create-contacts;;1"
    },
    {
      "cells": [
        "Sibusiso",
        "Zwane",
        "sz@gmail.com"
      ],
      "line": 17,
      "id": "sugarcrm-feature;create-contacts;;2"
    },
    {
      "cells": [
        "Relay",
        "Langa",
        "rl@jhb.dvt.com"
      ],
      "line": 18,
      "id": "sugarcrm-feature;create-contacts;;3"
    },
    {
      "cells": [
        "Thando",
        "Masondo",
        "tm@yahoo.com"
      ],
      "line": 19,
      "id": "sugarcrm-feature;create-contacts;;4"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 3319060800,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "I login with my username \u0027sw_khumalo\u0027 and password \u0027Sibusiso@2015\u0027 of SugarCRM",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "I should see my home page \u0027\u003cfirstname\u003e\u0027 \u0027\u003clastname\u003e\u0027",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "sw_khumalo",
      "offset": 26
    },
    {
      "val": "Sibusiso@2015",
      "offset": 52
    }
  ],
  "location": "SugarCRMStepDefs.i_login_with_my_username_swkhumalo_and_password_sibusiso2015_of_sugarcrm(String,String)"
});
formatter.result({
  "duration": 13521259600,
  "error_message": "java.lang.NullPointerException\r\n\tat BDD.step_definitions.SugarCRMStepDefs.checkResult(SugarCRMStepDefs.java:93)\r\n\tat BDD.step_definitions.SugarCRMStepDefs.i_login_with_my_username_swkhumalo_and_password_sibusiso2015_of_sugarcrm(SugarCRMStepDefs.java:45)\r\n\tat ✽.Given I login with my username \u0027sw_khumalo\u0027 and password \u0027Sibusiso@2015\u0027 of SugarCRM(Sample/SugarCRM1.feature:4)\r\n",
  "status": "failed"
});
formatter.match({
  "arguments": [
    {
      "val": "\u003cfirstname\u003e",
      "offset": 27
    },
    {
      "val": "\u003clastname\u003e",
      "offset": 41
    }
  ],
  "location": "SugarCRMStepDefs.i_should_see_my_home_page(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "line": 17,
  "name": "Create Contacts",
  "description": "",
  "id": "sugarcrm-feature;create-contacts;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 7,
      "name": "@CreateContact"
    }
  ]
});
formatter.step({
  "line": 9,
  "name": "I navigate to the Contacts page",
  "keyword": "Given "
});
formatter.step({
  "line": 10,
  "name": "I create a contact with first name Sibusiso and last name Zwane and email \u0027sz@gmail.com\u0027",
  "matchedColumns": [
    0,
    1,
    2
  ],
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "I should see the contact with first name Sibusiso Zwane is created",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 12,
  "name": "I logout of SugarCRM",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "I should see login page",
  "keyword": "Then "
});
formatter.match({
  "location": "SugarCRMStepDefs.i_navigate_to_the_contacts_page()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({
  "location": "SugarCRMStepDefs.i_logout_of_SugarCRM()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "SugarCRMStepDefs.i_should_see_login_page()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "duration": 673143900,
  "status": "passed"
});
formatter.before({
  "duration": 2665063700,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "I login with my username \u0027sw_khumalo\u0027 and password \u0027Sibusiso@2015\u0027 of SugarCRM",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "I should see my home page \u0027\u003cfirstname\u003e\u0027 \u0027\u003clastname\u003e\u0027",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "sw_khumalo",
      "offset": 26
    },
    {
      "val": "Sibusiso@2015",
      "offset": 52
    }
  ],
  "location": "SugarCRMStepDefs.i_login_with_my_username_swkhumalo_and_password_sibusiso2015_of_sugarcrm(String,String)"
});
formatter.result({
  "duration": 13774686200,
  "error_message": "java.lang.NullPointerException\r\n\tat BDD.step_definitions.SugarCRMStepDefs.checkResult(SugarCRMStepDefs.java:93)\r\n\tat BDD.step_definitions.SugarCRMStepDefs.i_login_with_my_username_swkhumalo_and_password_sibusiso2015_of_sugarcrm(SugarCRMStepDefs.java:45)\r\n\tat ✽.Given I login with my username \u0027sw_khumalo\u0027 and password \u0027Sibusiso@2015\u0027 of SugarCRM(Sample/SugarCRM1.feature:4)\r\n",
  "status": "failed"
});
formatter.match({
  "arguments": [
    {
      "val": "\u003cfirstname\u003e",
      "offset": 27
    },
    {
      "val": "\u003clastname\u003e",
      "offset": 41
    }
  ],
  "location": "SugarCRMStepDefs.i_should_see_my_home_page(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "line": 18,
  "name": "Create Contacts",
  "description": "",
  "id": "sugarcrm-feature;create-contacts;;3",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 7,
      "name": "@CreateContact"
    }
  ]
});
formatter.step({
  "line": 9,
  "name": "I navigate to the Contacts page",
  "keyword": "Given "
});
formatter.step({
  "line": 10,
  "name": "I create a contact with first name Relay and last name Langa and email \u0027rl@jhb.dvt.com\u0027",
  "matchedColumns": [
    0,
    1,
    2
  ],
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "I should see the contact with first name Relay Langa is created",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 12,
  "name": "I logout of SugarCRM",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "I should see login page",
  "keyword": "Then "
});
formatter.match({
  "location": "SugarCRMStepDefs.i_navigate_to_the_contacts_page()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({
  "location": "SugarCRMStepDefs.i_logout_of_SugarCRM()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "SugarCRMStepDefs.i_should_see_login_page()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "duration": 623736900,
  "status": "passed"
});
formatter.before({
  "duration": 2332971200,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "I login with my username \u0027sw_khumalo\u0027 and password \u0027Sibusiso@2015\u0027 of SugarCRM",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "I should see my home page \u0027\u003cfirstname\u003e\u0027 \u0027\u003clastname\u003e\u0027",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "sw_khumalo",
      "offset": 26
    },
    {
      "val": "Sibusiso@2015",
      "offset": 52
    }
  ],
  "location": "SugarCRMStepDefs.i_login_with_my_username_swkhumalo_and_password_sibusiso2015_of_sugarcrm(String,String)"
});
formatter.result({
  "duration": 16896249300,
  "error_message": "java.lang.NullPointerException\r\n\tat BDD.step_definitions.SugarCRMStepDefs.checkResult(SugarCRMStepDefs.java:93)\r\n\tat BDD.step_definitions.SugarCRMStepDefs.i_login_with_my_username_swkhumalo_and_password_sibusiso2015_of_sugarcrm(SugarCRMStepDefs.java:45)\r\n\tat ✽.Given I login with my username \u0027sw_khumalo\u0027 and password \u0027Sibusiso@2015\u0027 of SugarCRM(Sample/SugarCRM1.feature:4)\r\n",
  "status": "failed"
});
formatter.match({
  "arguments": [
    {
      "val": "\u003cfirstname\u003e",
      "offset": 27
    },
    {
      "val": "\u003clastname\u003e",
      "offset": 41
    }
  ],
  "location": "SugarCRMStepDefs.i_should_see_my_home_page(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "line": 19,
  "name": "Create Contacts",
  "description": "",
  "id": "sugarcrm-feature;create-contacts;;4",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 7,
      "name": "@CreateContact"
    }
  ]
});
formatter.step({
  "line": 9,
  "name": "I navigate to the Contacts page",
  "keyword": "Given "
});
formatter.step({
  "line": 10,
  "name": "I create a contact with first name Thando and last name Masondo and email \u0027tm@yahoo.com\u0027",
  "matchedColumns": [
    0,
    1,
    2
  ],
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "I should see the contact with first name Thando Masondo is created",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 12,
  "name": "I logout of SugarCRM",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "I should see login page",
  "keyword": "Then "
});
formatter.match({
  "location": "SugarCRMStepDefs.i_navigate_to_the_contacts_page()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({
  "location": "SugarCRMStepDefs.i_logout_of_SugarCRM()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "SugarCRMStepDefs.i_should_see_login_page()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "duration": 1243018100,
  "status": "passed"
});
});