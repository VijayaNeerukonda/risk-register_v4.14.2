/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author RNagel
 */
public class S5_Mail_Slurper_TestSuite extends BaseClass {

    static TestMarshall instance;

    public S5_Mail_Slurper_TestSuite() {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.MTN;

        //*******************************************
    }

    //SignInTo_MailSlurper v5
    @Test
    public void SignInTo_MailSlurper_V5() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5 - Test Pack");
        instance = new TestMarshall("TestPacks\\SignInTo_MailSlurper v5.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
}
