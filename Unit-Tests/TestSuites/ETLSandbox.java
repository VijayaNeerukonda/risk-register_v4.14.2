/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.DataRow;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Utilities.DataBaseUtility;
import KeywordDrivenTestFramework.Utilities.DataTableUtility;
import static java.lang.System.err;
import java.util.LinkedList;
import org.junit.Test;

/**
 *
 * @author Ferdinand
 */
public class ETLSandbox 
{
    @Test
    public void runETL()
    {
        Narrator narr = new Narrator();
        DataBaseUtility dbUtil = new DataBaseUtility();
       
        BaseClass.currentDatabase = Enums.Database.SourceData;
        String sourceSQLStatement = "SELECT * FROM APP.SOURCE_DATA";
        
        String destinationSQLStatement = "SELECT * FROM APP.DESTINATION_DATA";
        
        LinkedList<DataRow> sourceData, destinationData;
        sourceData = new LinkedList<>();
        destinationData = new LinkedList<>();
  
        sourceData = dbUtil.resultSetToArrayList(dbUtil.RunQuery(sourceSQLStatement));
        
        BaseClass.currentDatabase = Enums.Database.DestinationData;
        
        dbUtil = new DataBaseUtility();
  
        destinationData = dbUtil.resultSetToArrayList(dbUtil.RunQuery(destinationSQLStatement));
        
        DataTableUtility dtUtil = new DataTableUtility();
        ///Check M/F = 1/2 rule
        for(DataRow sourceRow : sourceData)
        {
            String userID,userGender, userGenderCode;
            userID = sourceRow.getColumnValue("USER_ID");
            
            DataRow destinationRow = dtUtil.getSpecificRowByColumnValue(destinationData, "USER_ID", userID);
            
            if(destinationRow == null)
            {
                err.println("[Source Id No - "+userID+"] Not found in Destination Table");
                continue;
            }
                    
            userGender = sourceRow.getColumnValue("USER_GENDER");
            userGenderCode = destinationRow.getColumnValue("USER_GENDER_CODE");
            
            if(userGender.equals("F") && userGenderCode.equals("1"))
            {
                System.out.println("[Source Id No - "+userID+"] Gender " + userGender + " Matched to Gender Code " + userGenderCode + " In destination table");
                
            }
            else if (userGender.equals("M") && userGenderCode.equals("2"))        
            {
                 System.out.println("[Source Id No - "+userID+"] Gender " + userGender + " Matched to Gender Code " + userGenderCode + " In destination table");
            }
            else
            {
                 System.err.println("[Source Id No - "+userID+"] Gender " + userGender + " Failed to Match to Gender Code " + userGenderCode + " In destination table");
            }   
                
        } 
            
        
    }
}
